/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.controller;

import cineuna.model.AsientoDto;
import cineuna.model.SalaDto;
import cineuna.service.SalaService;
import cineuna.util.AppContext;
import cineuna.util.ConverArchivo;
import cineuna.util.FlowController;
import cineuna.util.Mensaje;
import cineuna.util.Respuesta;
import cineuna.util.Seat;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXTextField;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

/**
 * FXML Controller class
 *
 * @author liedu
 */
public class EditorSalaController extends Controller implements Initializable {

    @FXML
    private Label lbNombreSala;
    @FXML
    private FlowPane fpTabla;
    @FXML
    private VBox vbSeleccion;

    @FXML
    private JFXButton btnModFondo;
    @FXML
    private JFXButton btnButacas;
    @FXML
    private VBox vbListButacas;
    @FXML
    private JFXButton btnTandas;
    @FXML
    private JFXButton btnGuardar;

    private ImageView imaFondo;
    private Integer cont;        //Contador para la enumeracion de la butaca
    private Integer letra;        //letra de la butaca.
    private ImageView imagen;    //imagen para la butaca.
    private Label butaca;   //Butaca que se va arrastrar.????
    private SalaDto sala;        //Sala en la que se esta trabajando.
    private Seat canasto;  //Variable que simula el movimiento.
    private Seat canasto1;
    public List<Seat> butacas; //Lista de asientos.
    private ObservableList<Seat> bases;
    private ArrayList<String> filas;
    private ArrayList<ImageView> imagenes;
    @FXML
    private JFXCheckBox ckActiva;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @Override
    public void initialize() {
        filas = new ArrayList();
        imagenes = new ArrayList();
        bases = FXCollections.observableArrayList();
        sala = (SalaDto) AppContext.getInstance().get("Sala");
        lbNombreSala.setText(sala.getSaNombre());
        BackgroundImage fondo = new BackgroundImage(ConverArchivo.stringToImage(sala.getSaFonImagen()), null, null, BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
        imaFondo = new ImageView(ConverArchivo.stringToImage(sala.getSaFonImagen()));
        fpTabla.setBackground(new Background(fondo));
        fpTabla.getChildren().clear();
        vbSeleccion.getChildren().clear();
        vbListButacas.getChildren().clear();
        filas.addAll(Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "I"));
        listaButacas();
        iniciarComponentes();
        generarBase();
        recuperar();
    }

    /**
     * Inicia los componentes
     */
    public void iniciarComponentes() {
        butacas = new ArrayList();
        btnButacas.setText("Butacas");
        btnButacas.setTooltip(new Tooltip("Elige el diseño de las butacas."));
        btnModFondo.setText("Fondo");
        btnModFondo.setTooltip(new Tooltip("Modificar la imagen de fondo de la sala"));
        imagen = new ImageView(imagenes.get(0).getImage());
        imagen.setFitHeight(60);
        imagen.setFitWidth(30);
        butaca = new Label();
        butaca.setGraphic(imagen);
        cargarEventoButaca(butaca);
        vbSeleccion.getChildren().add(butaca);
        butaca.setTooltip(new Tooltip("Siguiente butaca a colocar"));
    }

    public void recuperar() {
        System.out.println("Recuperado " + sala.getAsientos().size());
        for (AsientoDto asi : sala.getAsientos()) {
            String id = asi.getAsFila() + (asi.getAsNumAsiento() - 1);
            for (int i = 0; i < bases.size(); i++) {
                if (bases.get(i).getId().equals(id)) {
                    bases.get(i).setAsiento(asi);
                    BackgroundImage fondo = new BackgroundImage(imagen.getImage(), null, null, BackgroundPosition.CENTER, null);
                    bases.get(i).setBackground(new Background(fondo));
                    bases.get(i).setText(id);
                }
            }
        }
    }

    public void generarBase() {
        fpTabla.getChildren().clear();
        for (int i = 0; i < filas.size(); i++) {
            for (int j = 0; j < 16; j++) {
                Seat seat = new Seat(filas.get(i), j);
                seat.setId(filas.get(i) + j);
                seat.setPrefSize(37, 40);
                cargarEventoTabla(seat);
                fpTabla.getChildren().add(seat);
                bases.add(seat);
            }
        }
    }

    public void listaButacas() {
        ImageView a = new ImageView(new Image("cineuna/resources/butaca_1.png"));
        a.setOnMouseClicked(cImagen);
        ImageView b = new ImageView(new Image("cineuna/resources/butaca_2.png"));
        b.setOnMouseClicked(cImagen);
        ImageView c = new ImageView(new Image("cineuna/resources/butaca_3.png"));
        c.setOnMouseClicked(cImagen);
        imagenes.addAll(Arrays.asList(a, b, c));
        vbListButacas.getChildren().addAll(imagenes);
        vbListButacas.setVisible(false);
    }

    public void cargarEventoButaca(Label i) {
        i.setOnDragDetected(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                /* allow any transfer mode */
                Dragboard db = i.startDragAndDrop(TransferMode.COPY);
                /* put a image on dragboard */
                ClipboardContent content = new ClipboardContent();
                ImageView im = new ImageView(imagen.getImage());
                content.putImage(im.getImage());
                db.setContent(content);
                canasto = new Seat();
                canasto.setGraphic(im);
                canasto.setMaxSize(60, 30);
                canasto.setMinSize(60, 30);
                event.consume();
            }
        });

        i.setOnMouseEntered((MouseEvent e) -> {
            i.setCursor(Cursor.CLOSED_HAND);
        });
    }

    public void cargarEventoTabla(Seat pane) {

        pane.setOnDragOver((DragEvent event) -> {

            Dragboard db = event.getDragboard();
            if (db.hasImage()) {
                event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
            }

            event.consume();
        });

        pane.setOnDragDropped((DragEvent event) -> {
            Dragboard db = event.getDragboard();
            if (db.hasImage()) {
                Seat st = (Seat) event.getSource();
                st.getAsiento().setAsFila(st.getFila());
                st.getAsiento().setAsNumAsiento((st.getNumero() + 1));
                BackgroundImage fondo = new BackgroundImage(db.getImage(), null, null, BackgroundPosition.CENTER, null);
                st.setBackground(new Background(fondo));
                st.setText(st.getFila() + (st.getNumero() + 1));
                st.setStyle("-fx-alignment: CENTER");
                st.setOnMouseClicked(clic);
                butacas.add(st);
                event.setDropCompleted(true);
            } else {
                event.setDropCompleted(false);
            }

            event.consume();
        });
    }

    /**
     * Convierte de asiento-seat, seat-asiento
     *
     * @param n 2->asiento-seat 1->seat-asiento
     */
    public void convert(int n) {
        List<AsientoDto> asientos = new ArrayList();
        for (Seat st : butacas) {
            asientos.add(st.getAsiento());
        }
        sala.setAsientos(asientos);
    }

    @FXML
    private void modificarFondo(ActionEvent event) {
        if (Mensaje.showConfirmation("Alerta!!", getStage(), "La imagen que usted seleccione,\npodria afectar la apariencia de la interfaz.\n¿Desea continuar?")) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Buscar Imagen");
            // Agregar filtros para facilitar la busqueda
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("All Images", "*.*"),
                    new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                    new FileChooser.ExtensionFilter("PNG", "*.png")
            );
            // Obtener la imagen seleccionada
            File imgFile = fileChooser.showOpenDialog(getStage());
            // Mostar la imagen
            if (imgFile != null) {
                System.out.println("hollllalalala");
                ImageView imgImagen = new ImageView("file:" + imgFile.getAbsolutePath());
                sala.setSaFonImagen(ConverArchivo.imageToString(imgImagen.getImage(), "png"));
                BackgroundImage fondo = new BackgroundImage(ConverArchivo.stringToImage(sala.getSaFonImagen()), null, null, BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
                fpTabla.setBackground(new Background(fondo));
                imaFondo.setImage(imgImagen.getImage());
            }
        }
    }

    @FXML
    private void elegirButacas(ActionEvent event) {
        if (!vbListButacas.isVisible()) {
            vbListButacas.setVisible(true);
        } else {
            vbListButacas.setVisible(false);
        }
    }

    @FXML
    private void crearTandas(ActionEvent event) {
        FlowController.getInstance().goView("Tanda");
    }

    @FXML
    private void btnGuardar(ActionEvent event) {
        try {
            convert(1);
            sala.setSaFonImagen(ConverArchivo.imageToString(imaFondo.getImage(), "png"));
            sala.setSaEstado(estado());
            SalaService service = new SalaService();
            Respuesta resp = service.guardar(sala);
            if (resp.getEstado()) {
                Mensaje.showModal(Alert.AlertType.INFORMATION, "Guardado", getStage(), "Cambios gardados con exito");
                sala = (SalaDto) resp.getResultado("Sala");
            } else {
                Mensaje.showModal(Alert.AlertType.INFORMATION, "Guardado", getStage(), "Error al guardar los cambios");

            }

        } catch (Exception e) {
            Logger.getLogger(EditorSalaController.class.getName()).log(Level.SEVERE, "Error de guardado", e);
            Mensaje.showModal(Alert.AlertType.INFORMATION, "Guardado", getStage(), "Surgio un error inesperado, porfavor reportelo\n" + e.getMessage());
        }
    }

    public String estado() {
        if (ckActiva.isSelected()) {
            return "A";
        } else {
            return "I";
        }
    }

////////////////////////////////////////////////Eventos de clic///////////////////////////////////////////////
    EventHandler<MouseEvent> clic = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            Seat set = (Seat) event.getSource();
            System.out.println(set.getFila() + set.getNumero());
        }
    };

    EventHandler<MouseEvent> cImagen = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            ImageView img = (ImageView) event.getSource();
            imagen.setImage(img.getImage());
            butaca.setGraphic(new ImageView(img.getImage()));
            for (Seat but : butacas) {
                BackgroundImage fondo = new BackgroundImage(imagen.getImage(), null, null, BackgroundPosition.CENTER, null);
                but.setBackground(new Background(fondo));
            }
        }
    };

}//Fin clase
