/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.controller;

import cineuna.model.UsuarioDto;
import cineuna.service.UsuarioService;
import cineuna.util.AppContext;
import cineuna.util.FlowController;
import cineuna.util.Mensaje;
import cineuna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Emily
 */
public class RecuperarContrasenaController extends Controller implements Initializable {

    @FXML
    private Label lblNombre;
    @FXML
    private JFXTextField txtContrasena;
    @FXML
    private JFXTextField txtCompContra;
    @FXML
    private JFXButton btnGuardar;

    UsuarioDto usuario;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.usuario=(UsuarioDto) AppContext.getInstance().get("Usuario");
        lblNombre.setText(usuario.getUsNombre());
    }    

    @Override
    public void initialize() {
       
    }

    @FXML
    private void btnGuardar(MouseEvent event) {
        Guardar();
        
    }
    
    void Guardar(){
        if(txtCompContra.getText().isEmpty()||txtCompContra.getText()==""||txtContrasena.getText().isEmpty()||txtContrasena.getText()==""){
           
            Mensaje.showModal(Alert.AlertType.ERROR, "Cambio Contraseña", getStage(), "Debe ingresar su nueva contraseña y su confirmación");
        }else{
             if(txtCompContra.getText().equals(txtContrasena.getText())){
                this.usuario.setUsClave(txtContrasena.getText());
                this.usuario.setUsRecuperarClave("N");
                try {
            UsuarioService usuarioService = new UsuarioService();
            Respuesta res = usuarioService.guardar(this.usuario);
            if (!res.getEstado()) {
                Mensaje.showModal(Alert.AlertType.ERROR, "Cambio de contraseña", getStage(), res.getMensaje());
            } else {
                this.usuario = (UsuarioDto) res.getResultado("Usuario");
                Mensaje.showModal(Alert.AlertType.INFORMATION, "Cambio de contraseña", getStage(), "Cambio exitoso.");
                      FlowController.getInstance().delete("RecuperarContrasena");
                            FlowController.getInstance().goViewInWindow("LogIng");
                            ((Stage) this.lblNombre.getScene().getWindow()).close();
               
            }

        } catch (Exception ex) {
            Logger.getLogger(MantUsuarioController.class.getName()).log(Level.SEVERE, "Error cambiando la contraseña.", ex);
        }
            }else{
                Mensaje.showModal(Alert.AlertType.ERROR, "Cambio Contraseña", getStage(), "No coincide la contraseña con su confirmación");
            }
            
        }
    }
}
