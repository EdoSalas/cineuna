/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.controller;

import cineuna.model.PeliculaDto;
import cineuna.util.AppContext;
import cineuna.util.FlowController;
import cineuna.util.GuardarFondo;
import cineuna.util.Mensaje;
import com.jfoenix.controls.JFXButton;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Eduardo Salas Cerdas
 */
public class MenuAdminController extends Controller implements Initializable {

    @FXML
    private JFXButton btnPeliculas;
    @FXML
    private JFXButton btnLogOut;
    @FXML
    private JFXButton btnSalir;
    @FXML
    private JFXButton btnSalas;
    @FXML
    private JFXButton btnReporte;
    @FXML
    private JFXButton btnMantenimeintoCine;
    @FXML
    private JFXButton btnMantenimiento;
    @FXML
    private JFXButton btnCartelera;

    private String idioma;

   

  
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        inicializaComponentes();
        idioma = (String) AppContext.getInstance().get("idioma");
       
       
    }

    @Override
    public void initialize() {

    }

    public void inicializaComponentes() {

    }

    @FXML
    private void adminPelciulas(ActionEvent event) {
        FlowController.getInstance().goView("AdminPeliculas");
    }

    @FXML
    private void LogOut(ActionEvent event) {
        String titulo = "";

        String mensaje = "";
        if (idioma.equalsIgnoreCase("Es")) {
            titulo = "Salir";
            mensaje = "¿Desea cerrar sesion salir?";
        } else {
            titulo = "Go out";
            mensaje = "Do you want to log out?";
        }
        if (Mensaje.showConfirmation(titulo, getStage(), mensaje)) {
            AppContext.getInstance().set("nuevo", "NO");
            FlowController.getInstance().goLogIng();
            ((Stage) btnSalir.getScene().getWindow()).close();
        }

    }

    @FXML
    private void salir(ActionEvent event) {
        String titulo = "";

        String mensaje = "";
        if (idioma.equalsIgnoreCase("Es")) {
            titulo = "Salir";
            mensaje = "¿desea  salir?";
        } else {
            titulo = "Go out";
            mensaje = "Do you want to go out?";
        }
        if (Mensaje.showConfirmation(titulo, getStage(), mensaje)) {
            ((Stage) btnSalir.getScene().getWindow()).close();
        }

    }

    @FXML
    private void salas(ActionEvent event) {
        FlowController.getInstance().goView("Salas");
    }

    @FXML
    private void reportes(ActionEvent event) throws IOException {
        FlowController.getInstance().goView("Repor");

    }

    @FXML
    private void mantenimientoUsuarios(ActionEvent event) {
        FlowController.getInstance().goView("MantUsuario");
    }

    @FXML
    private void cartelera(ActionEvent event) {
        if ((WebView) AppContext.getInstance().get("web") != null) {
            ((WebView) AppContext.getInstance().get("web")).getEngine().load(null);
        }
        FlowController.getInstance().delete("Pelicula");
        FlowController.getInstance().goView("Carteleta2");
    }

    @FXML
    private void mantenimientoCine(ActionEvent event) {
        FlowController.getInstance().goView("MantCine");
    }

  

}
