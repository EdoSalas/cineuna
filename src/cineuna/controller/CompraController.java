/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.controller;

import cineuna.model.AsientoDto;
import cineuna.model.SalaDto;
import cineuna.model.TandaDto;
import cineuna.util.AppContext;
import cineuna.util.Seat;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.FlowPane;

/**
 * FXML Controller class
 *
 * @author Matami
 */
public class CompraController extends Controller implements Initializable {

    @FXML
    private Label lbNombreSala;
    @FXML
    private ImageView ImgFondo;
    @FXML
    private FlowPane fpTabla;

    private SalaDto sala;
    private TandaDto tanda;
    private ImageView imagen;
    private ArrayList<String> filas;
    private ObservableList<Seat> bases;
    private ArrayList<ImageView> imagenes;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @Override
    public void initialize() {
        iniciarComponentes();
        cargarVariables();
        generarBase();
        recuperar();

    }

    public void iniciarComponentes() {
        bases = FXCollections.observableArrayList();
        imagenes = new ArrayList();
        filas = new ArrayList();
        imagen = new ImageView();
        tanda = new TandaDto();
        sala = new SalaDto();
    }

    public void cargarVariables() {
        filas.addAll(Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "I"));
        tanda = (TandaDto) AppContext.getInstance().get("TandaCargada");
        sala = tanda.getTaIdSafk();
    }

    public void generarBase() {
        fpTabla.getChildren().clear();
        for (int i = 0; i < filas.size(); i++) {
            for (int j = 0; j < 16; j++) {
                Seat seat = new Seat(filas.get(i), j);
                seat.setId(filas.get(i) + j);
                seat.setPrefSize(37, 40);
                fpTabla.getChildren().add(seat);
//                seat.setText("Hola");
                bases.add(seat);
            }
        }
    }

    public void recuperar() {
        System.out.println("Recuperado " + sala.getAsientos().size());
        for (AsientoDto asi : sala.getAsientos()) {
            String id = asi.getAsFila() + (asi.getAsNumAsiento() - 1);
            for (int i = 0; i < bases.size(); i++) {
                if (bases.get(i).getId().equals(id)) {
                    bases.get(i).setAsiento(asi);
                    imagen = imagenes.get(asi.getAsNumAsiento());
                    BackgroundImage fondo = new BackgroundImage(imagen.getImage(), null, null, BackgroundPosition.CENTER, null);
                    bases.get(i).setBackground(new Background(fondo));
                    bases.get(i).setText(id);
                }
            }
        }
    }

    public void listaButacas() {
        ImageView a = new ImageView(new Image("cineuna/resources/butaca_1.png"));
        ImageView b = new ImageView(new Image("cineuna/resources/butaca_2.png"));
        ImageView c = new ImageView(new Image("cineuna/resources/butaca_3.png"));
        imagenes.addAll(Arrays.asList(a, b, c));
    }

}//Fin clase
