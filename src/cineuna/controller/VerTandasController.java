/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.controller;

import cineuna.model.PeliculaDto;
import cineuna.model.TandaDto;
import cineuna.service.TandaService;
import cineuna.util.AppContext;
import cineuna.util.ConverArchivo;
import cineuna.util.FlowController;
import cineuna.util.Mensaje;
import cineuna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Matami
 */
public class VerTandasController extends Controller implements Initializable {
    
    private List<TandaDto> tandas;
    
    private PeliculaDto peli;
    
    private List<JFXButton> botonPelicula = new ArrayList();
    
    @FXML
    private HBox hbTandas;
    @FXML
    private JFXDatePicker dpFecha;
    @FXML
    private JFXButton btnBuscar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }
    
    @Override
    public void initialize() {
        cargarDatos();
    }
    
    public void cargarDatos() {
        hbTandas.getChildren().clear();
        peli = (PeliculaDto) AppContext.getInstance().get("PeliTandas");
        if (peli != null) {
            TandaService tandaService = new TandaService();
            Respuesta res = tandaService.getTandas(peli.getPeId());
            if (!res.getEstado()) {
                //falta poner los mensajes
            } else {
                tandas = (List<TandaDto>) res.getResultado("Tandas");
                
                tandas.forEach((x) -> {
                    cargarTextoImagenPeli(x);
                });
            }
        }
    }
    
    public void cargarTextoImagenPeli(TandaDto tanda) {
        //cargar la peli en la sala
        tanda.setTaIdPefk(peli);
        JFXButton btn = new JFXButton();
        ImageView imgAc = new ImageView(ConverArchivo.stringToImage(peli.getPeImagenS()));
        //ajuste de la imagen tamanno
        imgAc.setFitWidth(130);
        imgAc.setFitHeight(100);
        //cargar imagen
        btn.setGraphic(imgAc);
        btn.setMinSize(100, 300);
        //css 
        btn.setStyle("-fx-font-size: 20px;-fx-background-radius: 30;-fx-content-display: top");
        //hora
        LocalTime hora = tanda.taFhinicio.getValue().toLocalTime();
        //fechas
        LocalDate feini = tanda.taFhinicio.getValue().toLocalDate();
        LocalDate fefin = tanda.taFhfin.getValue().toLocalDate();

        //cargar texto
        btn.setText("Disponible \n"
                + "               " + feini.toString() + "\n"
                + "              " + fefin.toString() + "\n"
                + "Hora:               " + hora.toString());

        //cargar elvento
        btn.setOnMouseClicked(clic);
        hbTandas.getChildren().add(btn);
        //comentario para botones tanda
        btn.setTooltip(new Tooltip("Click para ver mas infomacion  / Click to see more information"));
        btn.setId(tanda.getTaId().toString());
        botonPelicula.add(btn);
        
    }
    EventHandler<MouseEvent> clic = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            if (verificarFecha()) {
                JFXButton btn = (JFXButton) event.getSource();
                for (TandaDto tan : tandas) {
                    if (tan.getTaId().toString().equals(btn.getId())) {
                        AppContext.getInstance().set("FechaDeCompra", dpFecha.getValue());
                        AppContext.getInstance().set("TandaCargada", tan);
                        dpFecha.setValue(null);
                        FlowController.getInstance().goView("Compra");
                        
                    }
                    
                }
            }
        }
    };
    
    @FXML
    private void buscar(ActionEvent event) {
        if (verificarFecha()) {
            hbTandas.getChildren().clear();
            tandas.forEach((x) -> {
                if (x.taFhinicio.getValue().toLocalDate().toEpochDay() <= dpFecha.getValue().toEpochDay() && dpFecha.getValue().toEpochDay() <= x.taFhfin.getValue().toLocalDate().toEpochDay()) {
                    cargarTextoImagenPeli(x);
                }
            });
            dpFecha.setValue(null);
        }
    }
    
    public Boolean verificarFecha() {
        if (dpFecha.getValue() != null) {
            return true;
        } else {
            Mensaje.show(Alert.AlertType.CONFIRMATION, "", FlowController.getInstance().getLanguage().getString("msFechaV"));
            return false;
        }
    }
}
