/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.controller;

import cineuna.model.UsuarioDto;
import cineuna.util.AppContext;
import cineuna.util.ConverArchivo;
import cineuna.util.FlowController;
import cineuna.util.GuardarFondo;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Eduardo Salas Cerdas
 */
public class PrincipalController extends Controller implements Initializable {

    @FXML
    private JFXDrawer drawer;
    @FXML
    private JFXHamburger ham;
    @FXML
    private VBox vbxPrincipal;
    @FXML
    private BorderPane bpPrincipal;

    private GuardarFondo gf;

    private BackgroundImage fondo;
    @FXML
    private StackPane stpPrincipal;
    @FXML
    private ImageView ivFondo;
    
    private Boolean compra = false;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Necesario para la animacion de cambio
        AppContext.getInstance().set("centro", vbxPrincipal);
        AppContext.getInstance().set("FondoPrincipal", ivFondo);
        AppContext.getInstance().set("EnCompra", compra);
        GuardarFondo f = new GuardarFondo();
        ivFondo.setImage(f.getImagenFondo());
        // TODO
        if (((String) AppContext.getInstance().get("tipoUsuario")).equalsIgnoreCase("Administrador")) {
            abrirMenu("MenuAdmin");
        } else {
            abrirMenu("MenuCliente");
        }
        
    }

    @Override
    public void initialize() {

    }

    public void abrirMenu(String menu) {
        try {
            HBox hb = FXMLLoader.load(getClass().getResource("/cineuna/view/" + menu + ".fxml"), FlowController.getInstance().getLanguage());
//            hb.setAlignment(Pos.CENTER);
            this.drawer.setSidePane(hb);
        } catch (IOException ex) {
            Logger.getLogger(PrincipalController.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.ham.setOnMouseClicked((MouseEvent e) -> {
            if (this.drawer.isShown()) {
                this.drawer.close();
            } else {
                this.drawer.open();
            }
            e.consume();
        });
    }
}
