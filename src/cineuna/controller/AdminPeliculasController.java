/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.controller;

import cineuna.model.PeliculaDto;
import cineuna.model.SalaDto;
import cineuna.service.PeliculaService;
import cineuna.service.SalaService;
import cineuna.util.AppContext;
import cineuna.util.FlowController;
import cineuna.util.Mensaje;
import cineuna.util.Respuesta;
import cineuna.util.TablaPeliculas;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author Eduardo Salas Cerdas
 */
public class AdminPeliculasController extends Controller implements Initializable {

    @FXML
    private JFXButton btnAgregar;
    @FXML
    private JFXButton btnFiltrar;
    @FXML
    private JFXButton btnBuscar;
    @FXML
    private HBox hbFiltros;
    @FXML
    private JFXButton btnNombre;
    @FXML
    private JFXButton btnGenero;
    @FXML
    private JFXButton btnLimpiar;
    @FXML
    private HBox hbGenero;
    @FXML
    private JFXCheckBox ckAccion;
    @FXML
    private JFXCheckBox ckAventura;
    @FXML
    private JFXCheckBox ckComedia;
    @FXML
    private JFXCheckBox ckDrama;
    @FXML
    private JFXCheckBox ckTerror;
    @FXML
    private JFXCheckBox ckSuspenso;
    @FXML
    private JFXCheckBox ckMusical;
    @FXML
    private JFXCheckBox ckCienciaFiccion;
    @FXML
    private JFXCheckBox ckGuerra;
    @FXML
    private JFXCheckBox ckOeste;
    @FXML
    private JFXCheckBox ckInfantil;
    @FXML
    private HBox hbPublico;
    @FXML
    private JFXCheckBox ckNiños;
    @FXML
    private JFXCheckBox ckAdultos;
    @FXML
    private HBox hbNombre;
    @FXML
    private JFXTextField txtNombre;
    @FXML
    private TableView<TablaPeliculas> tvPeliculas;
    @FXML
    private TableColumn<TablaPeliculas, ImageView> tcImagen;
    @FXML
    private TableColumn<TablaPeliculas, String> tcNombre;
    @FXML
    private TableColumn<TablaPeliculas, String> tcGenero;
    @FXML
    private TableColumn<TablaPeliculas, String> tcActivo;
    @FXML
    private TableColumn<TablaPeliculas, JFXButton> tcEditar;

    private ObservableList<TablaPeliculas> obPeliculas;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @Override
    public void initialize() {
        inicializaComponentes();
        cargaPeliculas();
    }

    public void inicializaComponentes() {
        this.hbFiltros.setVisible(false);
        this.hbNombre.setVisible(false);
        this.hbGenero.setVisible(false);
        this.hbPublico.setVisible(false);
        this.hbNombre.setTranslateY(-90);
        this.tvPeliculas.setTranslateY(-45);
        this.obPeliculas = FXCollections.observableArrayList();

        //Tabla
        this.tcImagen.setCellValueFactory(new PropertyValueFactory("iv"));
        this.tcNombre.setCellValueFactory(new PropertyValueFactory("nombre"));
        this.tcGenero.setCellValueFactory(new PropertyValueFactory("genero"));
        this.tcActivo.setCellValueFactory(new PropertyValueFactory("activo"));
        this.tcEditar.setCellValueFactory(new PropertyValueFactory("buton"));
    }

    @FXML
    private void filtros(ActionEvent event) {
        this.hbFiltros.setVisible(!hbFiltros.isVisible());
        this.hbGenero.setVisible(false);
        this.hbPublico.setVisible(false);
        this.hbNombre.setVisible(false);
    }

    @FXML
    private void filtroNombre(ActionEvent event) {
        this.hbNombre.setVisible(!hbNombre.isVisible());
        if (hbGenero.isVisible()) {
            this.hbGenero.setVisible(false);
            this.hbPublico.setVisible(false);
        }
    }

    @FXML
    private void filtroGenero(ActionEvent event) {
        this.hbGenero.setVisible(!hbGenero.isVisible());
        this.hbPublico.setVisible(!hbPublico.isVisible());
        if (hbNombre.isVisible()) {
            this.hbNombre.setVisible(false);
        }
    }

    @FXML
    private void limpiarFiltros(ActionEvent event) {
        this.ckAccion.setSelected(false);
        this.ckAventura.setSelected(false);
        this.ckComedia.setSelected(false);
        this.ckDrama.setSelected(false);
        this.ckTerror.setSelected(false);
        this.ckSuspenso.setSelected(false);
        this.ckMusical.setSelected(false);
        this.ckCienciaFiccion.setSelected(false);
        this.ckGuerra.setSelected(false);
        this.ckOeste.setSelected(false);
        this.ckInfantil.setSelected(false);
        this.ckNiños.setSelected(false);
        this.ckAdultos.setSelected(false);
        this.txtNombre.setText("");
    }

    @FXML
    private void crear(ActionEvent event) {
        FlowController.getInstance().delete("AdminPeliculas");
        FlowController.getInstance().goView("NuevaPelicula");
    }

    public void cargaPeliculas() {
        try {
            String idioma = (String) AppContext.getInstance().get("idioma");
            PeliculaService ps = new PeliculaService();
            Respuesta respuesta = ps.getPeliculasList();
            this.obPeliculas.clear();
            this.tvPeliculas.getItems().clear();
            if (!respuesta.getEstado()) {
                if (idioma.equalsIgnoreCase("ES")) {
                    Mensaje.showModal(Alert.AlertType.ERROR, "Error al obtener la lista de peliculas.", getStage(), respuesta.getMensaje());
                } else {
                    Mensaje.showModal(Alert.AlertType.ERROR, "Failed to get the list of movies.", getStage(), respuesta.getMensaje());
                }
            } else {
                List<PeliculaDto> lis = (List<PeliculaDto>) respuesta.getResultado("Peliculas");
                if (lis != null) {
                    for (int i = 0; i < lis.size(); i++) {
                        //Image img = new Image(lis.get(i).getPeImagenS());
                        ImageView img = new ImageView(new Image("cineuna/resources/upload.png"));
                        img.setFitHeight(60);
                        img.setFitWidth(60);
                        Integer arroba = lis.get(i).getPeGenero().indexOf("@");
                        if (idioma.equalsIgnoreCase("ES")) {
                            String genero = lis.get(i).getPeGenero().substring(0, arroba);
                            this.obPeliculas.add(new TablaPeliculas(lis.get(i).getPeId(), img, lis.get(i).getPeNombreEs(), genero, lis.get(i).getPeEstado()));
                        } else {
                            String genero = lis.get(i).getPeGenero().substring(arroba + 1, lis.get(i).getPeGenero().length());
                            System.out.println(genero);
                            this.obPeliculas.add(new TablaPeliculas(lis.get(i).getPeId(), img, lis.get(i).getPeNombreIn(), genero, lis.get(i).getPeEstado()));
                        }
                    }
                    this.tvPeliculas.setItems(obPeliculas);
                } else {
                    Mensaje.showModal(Alert.AlertType.ERROR, "Error", getStage(), "Esta llegando null la lista de peliculas.");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(LogIngController.class.getName()).log(Level.SEVERE, "Error obteniendo la lista de peliculas.", ex);
        }
    }

    @FXML
    private void buscar(ActionEvent event) {
        try {
            if (this.txtNombre.getText().trim().length() > 0 || filtro()) {
                String idioma = (String) AppContext.getInstance().get("idioma");
                PeliculaService ps = new PeliculaService();
                Respuesta res = ps.getPeliculasList();
                this.obPeliculas.clear();
                this.tvPeliculas.getItems().clear();
                if (res.getEstado()) {
                    List<PeliculaDto> lis = (List<PeliculaDto>) res.getResultado("Peliculas");
                    if (lis != null) {
                        for (int i = 0; i < lis.size(); i++) {
                            filtrarNombre(lis, i, idioma);
                            filtrarGenero(lis, i, idioma);
                            filtrarNombreGenero();
                        }
                    } else {
                        Mensaje.showModal(Alert.AlertType.ERROR, "Error", getStage(), "Esta llegando null la lista de peliculas.");
                    }
                } else {
                    if (idioma.equalsIgnoreCase("ES")) {
                        Mensaje.showModal(Alert.AlertType.ERROR, "Error al obtener la lista de peliculas.", getStage(), res.getMensaje());
                    } else {
                        Mensaje.showModal(Alert.AlertType.ERROR, "Failed to get the list of movies.", getStage(), res.getMensaje());
                    }
                }
            } else {
                cargaPeliculas();
            }
        } catch (Exception ex) {
            Logger.getLogger(LogIngController.class.getName()).log(Level.SEVERE, "Error obteniendo la lista de peliculas.", ex);
        }
    }

    public void filtrarNombre(List<PeliculaDto> lis, int i, String idioma) {
        if (txtNombre.getText().trim().length() > 0) {
            if (lis.get(i).getPeNombreEs().contains(txtNombre.getText().trim()) || lis.get(i).getPeNombreIn().contains(txtNombre.getText().trim())) {
                ImageView img = new ImageView(new Image("cineuna/resources/upload.png"));
                Integer arroba = lis.get(i).getPeGenero().indexOf("@");
                if (idioma.equalsIgnoreCase("ES")) {
                    String genero = lis.get(i).getPeGenero().substring(0, arroba);
                    this.obPeliculas.add(new TablaPeliculas(lis.get(i).getPeId(), img, lis.get(i).getPeNombreEs(), genero, lis.get(i).getPeEstado()));
                } else {
                    String genero = lis.get(i).getPeGenero().substring(arroba + 1, lis.get(i).getPeGenero().length());
                    System.out.println(genero);
                    this.obPeliculas.add(new TablaPeliculas(lis.get(i).getPeId(), img, lis.get(i).getPeNombreIn(), genero, lis.get(i).getPeEstado()));
                }
            }
        }
    }

    public void filtrarGenero(List<PeliculaDto> lis, int i, String idioma) {
        if (filtro() && lis.get(i).getPeGenero().contains(filtros())) {
            ImageView img = new ImageView(new Image("cineuna/resources/upload.png"));
            Integer arroba = lis.get(i).getPeGenero().indexOf("@");
            if (idioma.equalsIgnoreCase("ES")) {
                String genero = lis.get(i).getPeGenero().substring(0, arroba);
                this.obPeliculas.add(new TablaPeliculas(lis.get(i).getPeId(), img, lis.get(i).getPeNombreEs(), genero, lis.get(i).getPeEstado()));
            } else {
                String genero = lis.get(i).getPeGenero().substring(arroba + 1, lis.get(i).getPeGenero().length());
                System.out.println(genero);
                this.obPeliculas.add(new TablaPeliculas(lis.get(i).getPeId(), img, lis.get(i).getPeNombreIn(), genero, lis.get(i).getPeEstado()));
            }
        }
    }

    public void filtrarNombreGenero(){
        eliminaRepetidos();
        ObservableList<TablaPeliculas> aux = FXCollections.observableArrayList();
        if(txtNombre.getText().trim().length() > 0 && filtro()){
            for(int i = 0; i < obPeliculas.size(); i++){
                if(obPeliculas.get(i).getNombre().contains(txtNombre.getText()) && obPeliculas.get(i).getGenero().contains(filtros())){
                    aux.add(obPeliculas.get(i));
                }
            }
            obPeliculas.clear();
            for(int i = 0; i < aux.size(); i++){
                    obPeliculas.add(aux.get(i));
            }
        }
        eliminaRepetidos();
    }
    
    public void eliminaRepetidos(){
        ObservableList<TablaPeliculas> aux = FXCollections.observableArrayList();
        Boolean agregado = false;
        for(int i = 0; i < obPeliculas.size(); i++){
            for(int j = 0;  j < aux.size(); j++){
                if(aux.get(j).getNombre().equalsIgnoreCase(obPeliculas.get(i).getNombre())){
                    agregado = true;
                }
            }
            if(!agregado){
                aux.add(obPeliculas.get(i));
            }
            agregado = false;
        }
        obPeliculas.clear();
        for(int i = 0; i < aux.size(); i++){
            obPeliculas.add(aux.get(i));
        }
    }
    
    public Boolean filtro() {
        return ckAccion.isSelected()
                || ckAventura.isSelected()
                || ckComedia.isSelected()
                || ckDrama.isSelected()
                || ckTerror.isSelected()
                || ckSuspenso.isSelected()
                || ckMusical.isSelected()
                || ckCienciaFiccion.isSelected()
                || ckGuerra.isSelected()
                || ckOeste.isSelected()
                || ckInfantil.isSelected();
    }

    public String filtros() {
        String filtro = "";
        if (ckAccion.isSelected()) {
            filtro += "Acción / ";
        }
        if (ckAventura.isSelected()) {
            filtro += "Aventura / ";
        }
        if (ckComedia.isSelected()) {
            filtro += "Comedia / ";
        }
        if (ckDrama.isSelected()) {
            filtro += "Drama / ";
        }
        if (ckTerror.isSelected()) {
            filtro += "Terror / ";
        }
        if (ckSuspenso.isSelected()) {
            filtro += "Suspenso / ";
        }
        if (ckMusical.isSelected()) {
            filtro += "Musical / ";
        }
        if (ckCienciaFiccion.isSelected()) {
            filtro += "Ciencia Ficción / ";
        }
        if (ckGuerra.isSelected()) {
            filtro += "Guerra / ";
        }
        if (ckOeste.isSelected()) {
            filtro += "Oeste / ";
        }
        if (ckInfantil.isSelected()) {
            filtro += "Infantil / ";
        }
        return filtro.substring(0, filtro.length() - 3);
    }
}
