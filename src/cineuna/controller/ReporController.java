/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.controller;

import cineuna.model.PeliculaDto;
import cineuna.model.ReporteDto;
import cineuna.service.PeliculaService;
import cineuna.service.TandaService;
import cineuna.util.AppContext;
import cineuna.util.ConverArchivo;
import cineuna.util.Mensaje;
import cineuna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author Matami
 */
public class ReporController extends Controller implements Initializable {

    @FXML
    private Tab txtTapPeli;
    @FXML
    private JFXButton btnReportePeliculas;
    @FXML
    private TableView<PeliculaDto> tvPeliculas;
    @FXML
    private TableColumn<PeliculaDto, SimpleStringProperty> tcNombre;
    @FXML
    private TableColumn<PeliculaDto, SimpleStringProperty> tcNombrein;
    @FXML
    private Tab txtTapTanda;
    @FXML
    private JFXDatePicker dpFecha2;
    @FXML
    private JFXDatePicker dpFecha1;
    @FXML
    private JFXButton tbnGenerarR2;

    private PeliculaDto peli;

    private ObservableList<PeliculaDto> peliculas = FXCollections.observableArrayList();
    @FXML
    private Label lbPeliculas;
    @FXML
    private HBox hbVisual;

    private String idioma;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        inicializarComponentes();
        cargarPelicuas();
        idioma = (String) AppContext.getInstance().get("idioma");
    }

    @Override
    public void initialize() {
        cargarPelicuas();
    }

    public void inicializarComponentes() {
        btnReportePeliculas.setText("Generar reporte para peliculas");
        tbnGenerarR2.setText("Generar reporte de tandas por fechas");
        txtTapPeli.setText("Peliculas reporte");
        txtTapTanda.setText("Tandas reporte");
        tcNombre.setText("Nombre");
        tcNombrein.setText("Name");
        lbPeliculas.setText("Seleccion de peliculas");

        //Cargar evento
        tvPeliculas.addEventHandler(MouseEvent.MOUSE_CLICKED, click2);
    }

    public void cargarPelicuas() {
        peliculas.clear();

        PeliculaService peli = new PeliculaService();
        Respuesta res = peli.getPeliculasList();
        if (!res.getEstado()) {
            Mensaje.show(Alert.AlertType.ERROR, "Cargar peliculas", "Lo sentimos, no se a podido cargar las peliculas");
        } else {
            List<PeliculaDto> p = (List<PeliculaDto>) res.getResultado("Peliculas");
            peliculas = FXCollections.observableArrayList(p);
            tcNombre.setCellValueFactory(new PropertyValueFactory<>("peNombreEs"));
            tcNombrein.setCellValueFactory(new PropertyValueFactory<>("peNombreIn"));
            tvPeliculas.setItems(peliculas);
        }

    }

    private void reporte(ActionEvent event) {

    }
/**
 * Genera el reporte en un rango de fechas
 * @param event 
 */
    @FXML
    private void generarPorFechas(ActionEvent event) {
        ReporteDto repot = new ReporteDto();
        if (datosNecesarios()) {
            repot.setFecha1(dpFecha1.getValue());
            repot.setFecha2(dpFecha2.getValue());
            TandaService tan = new TandaService();
            Respuesta res = tan.getReporteTanda(repot);

            if (!res.getEstado()) {
                if (idioma.equals("Es")) {
                    Mensaje.show(Alert.AlertType.ERROR, "Error", res.getMensaje());
                } else {
                    Mensaje.show(Alert.AlertType.ERROR, "Error", "Error loading the report");
                }
            } else {
                repot = (ReporteDto) res.getResultado("Reporte");
                String pr = ConverArchivo.stringToFile(repot.getArCode());
                // si el archivo esta vacio
                 if (repot.getArCode().equals("")) {
                    if (idioma.equalsIgnoreCase("Es")) {
                        Mensaje.show(Alert.AlertType.INFORMATION, "Información", "El archivo está vacío");
                    } else {
                        Mensaje.show(Alert.AlertType.INFORMATION, "Information", "The file is empty");
                    }
                } else {

                    try {
                        Desktop.getDesktop().open(new File(pr));//abrir el archivo en el el programa de por defecto
                    } catch (IOException ex) {

                        Logger.getLogger(LogIngController.class.getName()).log(Level.SEVERE, null, ex);
                        Mensaje.show(Alert.AlertType.ERROR, "Error", "Lo sentimos en estos momentos no podemos mostrar el reporte");
                    }
                }
            }
        } else {
            if (idioma.equals("Es")) {
                Mensaje.show(Alert.AlertType.ERROR, "Datos", "Es necesario que ingrese los dos parametros de horas");
            } else {
                Mensaje.show(Alert.AlertType.ERROR, "Data", "It is necessary that you enter the two hours parameters");
            }
        }
    }

    public Boolean datosNecesarios() {
        if (dpFecha1.getValue() == null) {
            return false;
        } else {
            if (dpFecha2.getValue() == null) {
                return false;
            } else {
                return true;
            }
        }
    }
    /**
     * genera el reporte de una pelicula en especifico 
     * @param event 
     */
    @FXML
    private void reportePeliculas(ActionEvent event) {
        if (peli != null) {
            Mensaje.show(Alert.AlertType.INFORMATION, "Aviso", "Un momento por favor!");
            PeliculaService pel = new PeliculaService();
            Respuesta res = pel.getReportePeli(peli.getPeId());
            if (res.getEstado()) {
                ReporteDto repor = (ReporteDto) res.getResultado("Reporte");
                String pr = ConverArchivo.stringToFile(repor.getArCode());
                if (repor.getArCode().equals("")) {
                    if (idioma.equalsIgnoreCase("Es")) {
                        Mensaje.show(Alert.AlertType.INFORMATION, "Información", "El archivo está vacío");
                    } else {
                        Mensaje.show(Alert.AlertType.INFORMATION, "Information", "The file is empty");
                    }
                } else {

                    try {
                        Desktop.getDesktop().open(new File(pr));//abrir el archivo en el el programa de por defecto
                    } catch (IOException ex) {

                        Logger.getLogger(LogIngController.class.getName()).log(Level.SEVERE, null, ex);
                        Mensaje.show(Alert.AlertType.ERROR, "Error", "Lo sentimos en estos momentos no podemos mostrar el reporte");
                    }
                }
            } else {
                if (idioma.equals("Es")) {
                    Mensaje.show(Alert.AlertType.ERROR, "Error", res.getMensaje());
                } else {
                    Mensaje.show(Alert.AlertType.ERROR, "Error", "Error loading the report");
                }
            }
        }
    }

    //**********************************************************    
    EventHandler<MouseEvent> click2 = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            peli = getSeleccionada();
        }
    };

    /**
     * // * PARA SELECCIONAR UNA CELDA DE LA TABLA "tablaCliente" //
     */
    public PeliculaDto getSeleccionada() {
        if (tvPeliculas != null) {
            List<PeliculaDto> tabla = tvPeliculas.getSelectionModel().getSelectedItems();
            if (tabla.size() == 1) {
                final PeliculaDto competicionSeleccionada = tabla.get(0);
                return competicionSeleccionada;
            }
        }
        return null;
    }

}
