/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.controller;

import cineuna.model.CineDto;
import cineuna.service.CineService;
import cineuna.util.AppContext;
import cineuna.util.Mensaje;
import cineuna.util.Respuesta;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTimePicker;
import java.net.URL;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

/**
 * FXML Controller class
 *
 * @author Emily
 */
public class InfoCineController extends Controller implements Initializable {

    @FXML
    private JFXTextField txtNomCine;
    @FXML
    private JFXTextField txtTelefono;
    @FXML
    private JFXTextField txtCorreo;
    @FXML
    private JFXTimePicker tpIni;
    @FXML
    private JFXTimePicker tbFin;
    
    private List<CineDto> listcines;
    CineDto cine;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
      initialize();
    }    
    
    public void cargarDatos(){
        try {
            CineService cineService = new CineService();
            Respuesta respuesta = cineService.getCines();
            if (respuesta.getEstado()) {
                AppContext.getInstance().set("Cines",(List<CineDto>) respuesta.getResultado("Cines"));
                listcines =  (List<CineDto>) AppContext.getInstance().get("Cines");
            } else {
                Mensaje.showModal(Alert.AlertType.ERROR, "Cargar", getStage(), respuesta.getMensaje());
            }
        } catch (Exception ex) {
            Logger.getLogger(LogIngController.class.getName()).log(Level.SEVERE, "Error ingresando.", ex);
        }

        if (listcines.size() > 0) {
            cine = listcines.get(0);
            AppContext.getInstance().set("Cine", this.cine);
            
            txtNomCine.setText(cine.getcNombre());
            txtCorreo.setText(cine.getCorreo());
            txtTelefono.setText(cine.getcTelefono().toString());
            tpIni.setValue(cine.cHorai.getValue().toLocalTime());
            tbFin.setValue(cine.cHoraf.getValue().toLocalTime());
           
        }
    }

    @Override
    public void initialize() {
        listcines=new ArrayList<>();
      cargarDatos();
    }
}
