/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.controller;

import cineuna.model.PeliculaDto;
import cineuna.model.SalaDto;
import cineuna.model.TandaDto;
import cineuna.service.PeliculaService;
import cineuna.service.TandaService;
import cineuna.util.AppContext;
import cineuna.util.ConverArchivo;
import cineuna.util.Formato;
import cineuna.util.Mensaje;
import cineuna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTimePicker;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author liedu
 */
public class TandaController extends Controller implements Initializable {

    @FXML
    private JFXComboBox<?> cbPeliculas;
    @FXML
    private JFXTextField txtsalas;
    @FXML
    private JFXTextField txtNombre;
    @FXML
    private ImageView imgPelicula;
    @FXML
    private JFXDatePicker dpInicio;
    @FXML
    private JFXDatePicker dpFinal;
    @FXML
    private JFXTimePicker tpHora;
    @FXML
    private JFXButton btnGuardar;
    @FXML
    private JFXButton btnEliminar;
    @FXML
    private TableView<TandaDto> tvTandas;
    @FXML
    private TableColumn<TandaDto, SimpleStringProperty> tcPelicula;
    @FXML
    private TableColumn<TandaDto, SimpleStringProperty> tcHora;
    @FXML
    private TableColumn<TandaDto, SimpleStringProperty> tcHoraInico;
    @FXML
    private TableColumn<TandaDto,SimpleStringProperty> tcHoraFin;

    private TandaDto tanda;
    private PeliculaDto pelicula;
    private SalaDto sala;
    private ObservableList<TandaDto> tandas;
    private List<PeliculaDto> peliculas;
    @FXML
    private ToggleGroup Idioma;
    @FXML
    private JFXTextField txtPrecio;
    @FXML
    private JFXButton btnLimpiar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @Override
    public void initialize() {
        iniciarComponentes();
        recuperarDatos();
        cargarDatos();
    }

    public void iniciarComponentes() {
        //Variables
        pelicula = new PeliculaDto();
        sala = new SalaDto();
        peliculas = new ArrayList();
        tandas = FXCollections.observableArrayList();

        //Conponentes
        txtNombre.setPromptText("Pelicula");
        txtsalas.setPromptText("Salas");
        txtNombre.setLabelFloat(true);
        txtPrecio.setPromptText("Costo");
        txtPrecio.setLabelFloat(true);
        txtPrecio.textFormatterProperty().set(Formato.getInstance().integerFormat());

        tcPelicula.setText("Pelicula");
        tcHoraFin.setText("Hora fin");
        tcHoraInico.setText("Hora de inicio");
        tcHora.setText("Costo");
          //   colApellido.setCellValueFactory((param) -> new SimpleStringProperty(param.getValue().getUsApellido()));
        tcPelicula.setCellValueFactory(new PropertyValueFactory<>("taIdPefk"));
        tcHoraFin.setCellValueFactory(new PropertyValueFactory<>("taFhfin"));
        tcHoraInico.setCellValueFactory(new PropertyValueFactory<>("taFhinicio"));
        tcHora.setCellValueFactory(new PropertyValueFactory<>("taCosto"));

        tvTandas.addEventHandler(MouseEvent.MOUSE_CLICKED, click2);

    }

    public void cargarDatos() {
        cbPeliculas.setItems(listToObser(peliculas));
        tvTandas.setItems(tandas);
        txtsalas.setText(sala.getSaNombre());
    }

    public void cargarTanda(TandaDto td) {
        txtPrecio.setText(td.getTaCosto().toString());
        dpFinal.setValue(td.taFhfin.getValue().toLocalDate());
        dpInicio.setValue(td.taFhinicio.getValue().toLocalDate());
        tpHora.setValue(td.taFhfin.getValue().toLocalTime());
    }

    public void recuperarDatos() {
        try {
            sala = (SalaDto) AppContext.getInstance().get("Sala");
            tandas.addAll(sala.getTandas());
            System.out.println(sala.getTandas());
            PeliculaService service = new PeliculaService();
            Respuesta resp = service.getPeliculasList();
            if (resp.getEstado()) {
                peliculas = (List<PeliculaDto>) resp.getResultado("Peliculas");
            }
        } catch (Exception ex) {
            Logger.getLogger(LogIngController.class.getName()).log(Level.SEVERE, null, ex);
            Mensaje.show(Alert.AlertType.ERROR, "Error", "Lo sentimos, surgio un error inesperado, por favor comuniquelo");

        }
    }

    public void nuevaTanda() {
        tanda = new TandaDto();
        if (cbPeliculas.getValue() != null) {
            tanda.setTaIdPefk(pelicula);
            tanda.setTaIdSafk(sala);
        } else {
            Mensaje.showModal(Alert.AlertType.ERROR, "Error al guardar", getStage(), "Ingrese una pelicula");
        }

        if (tpHora.getValue() != null && dpInicio.getValue() != null && dpFinal.getValue() != null) {
            tanda.taFhinicio.set(LocalDateTime.of(dpInicio.getValue(), tpHora.getValue()));
            tanda.taFhfin.set(LocalDateTime.of(dpFinal.getValue(), tpHora.getValue()));
        } else {
            Mensaje.showModal(Alert.AlertType.ERROR, "Error al guardar", getStage(), "Verifique la hora o las fechas");
        }

        if (!txtPrecio.equals("")) {
            tanda.setTaCosto(Integer.parseInt(txtPrecio.getText()));
        } else {
            Mensaje.showModal(Alert.AlertType.ERROR, "Error al guardar", getStage(), "Ingrese un costo");

        }
        tanda.setTaIdioma("Es");
    }

    public void limpiarCampos() {
        txtNombre.clear();
        txtPrecio.clear();
        imgPelicula.setImage(null);
        dpFinal.setValue(null);
        dpInicio.setValue(null);
        tpHora.setValue(null);
        cbPeliculas.getSelectionModel().clearSelection();
    }

    public ObservableList listToObser(List<PeliculaDto> list) {
        ObservableList ob;
        ob = FXCollections.observableArrayList();
        for (PeliculaDto peli : list) {
            ob.add(peli);
        }
        return ob;
    }

    @FXML
    private void Eliminar(ActionEvent event) {

    }

    @FXML
    private void Guardar(ActionEvent event) {
        nuevaTanda();
        TandaService service = new TandaService();
        Respuesta resp = service.guardarTanda(tanda);
        if (resp.getEstado()) {
            tandas.add((TandaDto) resp.getResultado("Tanda"));
            limpiarCampos();
        } else {
            Mensaje.showModal(Alert.AlertType.ERROR, "Error al guardar", getStage(), "Error al guardar la tanda..." + resp.getMensaje());
        }
    }

    @FXML
    private void cargarPelicula(ActionEvent event) {
        pelicula = (PeliculaDto) cbPeliculas.getValue();
        txtNombre.setText(pelicula.getPeNombreEs());
        imgPelicula.setImage(ConverArchivo.stringToImage(pelicula.getPeImagenS()));
    }

    EventHandler<MouseEvent> click2 = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            tanda = getSeleccionada();
            cargarTanda(tanda);
        }
    };

    public TandaDto getSeleccionada() {
        if (tvTandas != null) {
            List<TandaDto> tabla = tvTandas.getSelectionModel().getSelectedItems();
            if (tabla.size() == 1) {
                final TandaDto competicionSeleccionada = tabla.get(0);
                return competicionSeleccionada;
            }
        }
        return null;
    }

    @FXML
    private void limpiar(ActionEvent event) {
        limpiarCampos();
    }

}//Fin clase.
