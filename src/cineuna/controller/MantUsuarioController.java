/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.controller;

import cineuna.model.UsuarioDto;
import cineuna.service.UsuarioService;
import cineuna.util.AppContext;
import cineuna.util.FlowController;
import cineuna.util.Formato;
import cineuna.util.Mensaje;
import cineuna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Emily
 */
public class MantUsuarioController extends Controller implements Initializable {

    @FXML
    private JFXTextField txtNombre;
    @FXML
    private JFXTextField txtNomUsuario;
    @FXML
    private JFXTextField txtCedula;
    @FXML
    private JFXTextField txtApellido;

    @FXML
    private JFXTextField txtCorreo;
    @FXML
    private JFXTextField txtTelefono;
    @FXML
    private ComboBox<String> CbbIdiomas;
    @FXML
    private ComboBox<String> cbbGenero;
    @FXML
    private JFXTextField txtVerLista;

    @FXML
    private JFXButton btnModificar;
    @FXML
    private JFXButton btnGuardar;
    @FXML
    private JFXButton btnDesactivarUsuario;

    private final ArrayList<JFXTextField> req = new ArrayList();
    private UsuarioDto usuario;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        initialize();

    }

    @FXML
    private void AdmListaUsuarios(MouseEvent event) {
        FlowController.getInstance().goView("MantListUsuarios");
    }

    @FXML
    private void Modificar(MouseEvent event) {
        txtApellido.setDisable(false);
        txtNomUsuario.setDisable(false);
        txtNombre.setDisable(false);
        txtTelefono.setDisable(false);

        CbbIdiomas.setDisable(false);
        cbbGenero.setDisable(false);
        btnGuardar.setDisable(false);
    }

    @FXML
    private void guardar(MouseEvent event) {

        guardar();
    }

    @FXML
    private void desactivarUsuario(MouseEvent event) {
        List<UsuarioDto> ListUsuarios;
        UsuarioService usuarioService1 = new UsuarioService();
        Respuesta resp = usuarioService1.getUsuariosActivos();
        ListUsuarios = (List<UsuarioDto>) resp.getResultado("Usuarios");
        int adm = 0;

        for (int i = 0; i < ListUsuarios.size(); i++) {
            if (ListUsuarios.get(i).usAdministrador.getValue().equals("A")) {
                adm++;
            }
        }
        if (adm == 1) {
            Mensaje.showModal(Alert.AlertType.ERROR, "", getStage(), FlowController.getInstance().getLanguage().getString("msadmUnico"));
        } else if(Mensaje.showConfirmation(FlowController.getInstance().getLanguage().getString("msDesactivar"), getStage(), FlowController.getInstance().getLanguage().getString("msDesac"))){
            try {
                 usuario.setUsActivo("I");
                UsuarioService usuarioService = new UsuarioService();
                Respuesta res = usuarioService.guardar(usuario);
                if (!res.getEstado()) {

                    Mensaje.showModal(Alert.AlertType.ERROR, FlowController.getInstance().getLanguage().getString("msDesactivar"), getStage(), res.getMensaje());
                } else {

                    Mensaje.showModal(Alert.AlertType.INFORMATION, FlowController.getInstance().getLanguage().getString("msDesactivar"), getStage(), FlowController.getInstance().getLanguage().getString("msDesactivarI"));
                    AppContext.getInstance().set("nuevo", "NO");
                    FlowController.getInstance().goLogIng();
                    ((Stage) btnGuardar.getScene().getWindow()).close();

                }

            } catch (Exception ex) {

                Logger.getLogger(MantUsuarioController.class.getName()).log(Level.SEVERE, FlowController.getInstance().getLanguage().getString("msModificarE"), ex);
            }
        }
    }

    public void cargarDatos() {

        txtNombre.setText(usuario.getUsNombre());
        txtApellido.setText(usuario.getUsApellido());
        txtCedula.setText(usuario.getUsCedula());

        txtCorreo.setText(usuario.getUsCorreo());
        txtNomUsuario.setText(usuario.getUsUsuario());
        txtTelefono.setText(usuario.getUsTelefono().toString());

        String usu = (String) AppContext.getInstance().get("tipoUsuario");
        if (usuario.getUsAdministrador().equals("A") && !usu.equals("Cliente")) {
            txtVerLista.setDisable(false);
            txtVerLista.setVisible(true);
        }

        CbbIdiomas.setPromptText(FlowController.getInstance().getLanguage().getString("rb.IdiomaU"));

        if (usuario.getUsGenero().equals("F")) {
            cbbGenero.setPromptText(FlowController.getInstance().getLanguage().getString("rb.Femenino"));
        } else {
            cbbGenero.setPromptText(FlowController.getInstance().getLanguage().getString("rb.Masculino"));
        }

        if (usuario.getUsAdministrador().equals("A")) {
            txtVerLista.setVisible(true);
            txtVerLista.setDisable(false);
        }
    }

    public void guardar() {
        boolean validar = validar();
        if (validar) {

            Mensaje.showModal(Alert.AlertType.ERROR, FlowController.getInstance().getLanguage().getString("msDatosIT"), getStage(), FlowController.getInstance().getLanguage().getString("msDatosIM"));

        } else {
            Boolean disponible = true;

            if (!txtNomUsuario.getText().equals(usuario.getUsUsuario())) {
                UsuarioService usuarioService = new UsuarioService();
                Respuesta res = usuarioService.validarUsuario(txtNomUsuario.getText());
                boolean org = (boolean) res.getEstado();
                if (!org) {
                    disponible = true;
                } else {
                    disponible = false;
                }
            }

            if (disponible) {
                usuario = cambiarDatos(usuario);
                guardar2(usuario);
            } else {

                Mensaje.showModal(Alert.AlertType.ERROR, FlowController.getInstance().getLanguage().getString("msModificar"), getStage(), FlowController.getInstance().getLanguage().getString("msNomUsuI"));

            }

        }

    }

    public void inicializar() {
        cbbGenero.getItems().clear();
        cbbGenero.getItems().addAll(FlowController.getInstance().getLanguage().getString("rb.Masculino"), FlowController.getInstance().getLanguage().getString("rb.Femenino"));
        CbbIdiomas.getItems().clear();
        CbbIdiomas.getItems().addAll("Español", "English");
        txtApellido.textFormatterProperty().set(Formato.getInstance().letrasFormat(40));
        txtCedula.textFormatterProperty().set(Formato.getInstance().cedulaFormat(20));

        txtNombre.textFormatterProperty().set(Formato.getInstance().letrasFormat(40));
        txtTelefono.textFormatterProperty().set(Formato.getInstance().integerFormat());
        txtNomUsuario.textFormatterProperty().set(Formato.getInstance().maxLengthFormat(20));
        txtCorreo.textFormatterProperty().set(Formato.getInstance().maxLengthFormat(40));
        cargarDatos();
    }

    @Override
    public void initialize() {
        usuario = (UsuarioDto) AppContext.getInstance().get("Usuario");

        inicializar();

        btnModificar.setText(FlowController.getInstance().getLanguage().getString("btn.ModificarU"));
        btnDesactivarUsuario.setText(FlowController.getInstance().getLanguage().getString("btn.DesactivarUsuarioU"));
        btnGuardar.setText(FlowController.getInstance().getLanguage().getString("btn.GuardarU"));
        txtVerLista.setText(FlowController.getInstance().getLanguage().getString("txt.VerListaU"));
        txtNomUsuario.setPromptText(FlowController.getInstance().getLanguage().getString("lbl.NomUs"));
        txtApellido.setPromptText(FlowController.getInstance().getLanguage().getString("txt.Apellido"));
        txtCedula.setPromptText(FlowController.getInstance().getLanguage().getString("txt.Cedula"));
        txtCorreo.setPromptText(FlowController.getInstance().getLanguage().getString("txt.Correo"));
        txtNombre.setPromptText(FlowController.getInstance().getLanguage().getString("txt.Nombre"));
        txtTelefono.setPromptText(FlowController.getInstance().getLanguage().getString("txt.Telefono"));
        btnDesactivarUsuario.setTooltip(new Tooltip("Desactivación de la cuenta del usuario. \n Deactivation of the user's account."));
        btnGuardar.setTooltip(new Tooltip("Guardar las modificaciones hechas en la información del usuario. \n Save the modifications made to the user."));
        btnModificar.setTooltip(new Tooltip("Realizar modificaciones en la información del usuario. \n Make modifications to the user's information"));
    }

    public void requeridos() {
        req.clear();
        req.addAll(Arrays.asList(txtCedula, txtApellido, txtNombre, txtTelefono, txtNomUsuario));

    }

    public boolean validar() {
        requeridos();
        Boolean req1 = false;

        for (JFXTextField x : this.req) {
            if (x.getText().isEmpty() || x.equals(" ")) {
                req1 = true;
            }
        }
        return req1;
    }

    private UsuarioDto cambiarDatos(UsuarioDto usuario) {

        usuario.setUsApellido(txtApellido.getText());
        usuario.setUsCedula(txtCedula.getText());
        usuario.setUsCorreo(txtCorreo.getText());
        usuario.setUsNombre(txtNombre.getText());
        usuario.setUsTelefono(Integer.valueOf(txtTelefono.getText()));
        usuario.setUsUsuario(txtNomUsuario.getText());

        if (CbbIdiomas.getSelectionModel().getSelectedItem() != null) {
            if (CbbIdiomas.getSelectionModel().getSelectedItem().equals("Español")) {
                usuario.setUsIdioma("Es");
            } else {
                usuario.setUsIdioma("In");
            }
        }

        if (cbbGenero.getSelectionModel().getSelectedItem() != null) {
            if (cbbGenero.getSelectionModel().getSelectedItem().equals("Femenino") || cbbGenero.getSelectionModel().getSelectedItem().equals("Female")) {
                usuario.setUsGenero("F");
            } else {
                usuario.setUsGenero("M");
            }
        }

        return usuario;
    }

    private void guardar2(UsuarioDto usu) {
        try {
            UsuarioService usuarioService = new UsuarioService();
            Respuesta res = usuarioService.guardar(usu);
            if (!res.getEstado()) {

                Mensaje.showModal(Alert.AlertType.ERROR, FlowController.getInstance().getLanguage().getString("msModificar"), getStage(), res.getMensaje());

            } else {
                usuario = (UsuarioDto) res.getResultado("Usuario");
                AppContext.getInstance().set("idioma", usuario.getUsIdioma());
                if (usuario.usIdioma.getValue().equals("Es")) {
                    FlowController.getInstance().loadLanguage(ResourceBundle.getBundle("cineuna/resources/i18n/es"));
                } else {
                    FlowController.getInstance().loadLanguage(ResourceBundle.getBundle("cineuna/resources/i18n/en"));
                }

                Mensaje.showModal(Alert.AlertType.INFORMATION, FlowController.getInstance().getLanguage().getString("msModificar"), getStage(), FlowController.getInstance().getLanguage().getString("msModExi"));
                desactivarComponentes();
                initialize();
            }

        } catch (Exception ex) {

            Logger.getLogger(MantUsuarioController.class.getName()).log(Level.SEVERE, FlowController.getInstance().getLanguage().getString("msModErr"), ex);

        }
    }

    private void desactivarComponentes() {
        btnGuardar.setDisable(false);
        txtApellido.setDisable(false);
        txtCedula.setDisable(false);
        txtNomUsuario.setDisable(false);
        txtTelefono.setDisable(false);
        txtNombre.setDisable(false);
        cbbGenero.setDisable(false);
        CbbIdiomas.setDisable(false);
    }
}
