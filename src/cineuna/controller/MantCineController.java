/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.controller;

import cineuna.model.CineDto;
import cineuna.model.UsuarioDto;
import cineuna.service.CineService;
import cineuna.util.AppContext;
import cineuna.util.FlowController;
import cineuna.util.Formato;
import cineuna.util.GuardarFondo;
import cineuna.util.Mensaje;
import cineuna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTimePicker;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;

/**
 * FXML Controller class
 *
 * @author Emily
 */
public class MantCineController extends Controller implements Initializable {

    @FXML
    private JFXTextField txtNomCine;
    @FXML
    private JFXTextField txtTelefono;
    @FXML
    private JFXTextField txtCorreo;
    @FXML
    private JFXTimePicker TimePInicio;
    @FXML
    private JFXTimePicker TimePFin;
    @FXML
    private JFXButton btnGuardar;
    @FXML
    private JFXButton btnModificar;

    @FXML
    private JFXTextField txtIniCorreo;
    @FXML
    private JFXComboBox<?> cmbEmpre;
    @FXML
    private JFXComboBox<?> cmbDominio;
    @FXML
    private JFXButton btnModifCorreo;
    @FXML
    private JFXButton btnCambiarCorreo;

    private ObservableList<String> empresa;
    private ObservableList<String> dominio;

    private CineDto cine;

    private ArrayList<JFXTextField> req;

    private List<CineDto> listcines;

    private boolean nuevo;

    private GuardarFondo f = new GuardarFondo();

    private JFXButton btnCambioFondo;

    private ImageView imgPelicula;
    @FXML
    private JFXButton btncambioFondo;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        imgPelicula = (ImageView) AppContext.getInstance().get("FondoPrincipal");
        btncambioFondo.setTooltip(new Tooltip(FlowController.getInstance().getLanguage().getString("msTool")));
        initialize();
    }

    @Override
    public void initialize() {
        req = new ArrayList();
        listcines = new ArrayList();
        this.nuevo = false;
        txtNomCine.textFormatterProperty().set(Formato.getInstance().letrasFormat(20));
        txtCorreo.textFormatterProperty().set(Formato.getInstance().letrasFormat(40));
        txtTelefono.textFormatterProperty().set(Formato.getInstance().integerFormat());

        CargarDatos();

    }

    public void CargarDatos() {
        try {
            CineService cineService = new CineService();
            Respuesta respuesta = cineService.getCines();
            if (respuesta.getEstado()) {
                AppContext.getInstance().set("Cines", (List<CineDto>) respuesta.getResultado("Cines"));
                listcines = (List<CineDto>) AppContext.getInstance().get("Cines");
            } else {
                Mensaje.showModal(Alert.AlertType.ERROR, "Cargar", getStage(), respuesta.getMensaje());
            }
        } catch (Exception ex) {
            Logger.getLogger(LogIngController.class.getName()).log(Level.SEVERE, "Error ingresando.", ex);
        }

        if (listcines.size() > 0) {
            cine = listcines.get(0);
            AppContext.getInstance().set("Cine", this.cine);

            txtNomCine.setText(cine.getcNombre());
            txtCorreo.setText(cine.correo.getValue());
            txtTelefono.setText(cine.getcTelefono().toString());
            TimePFin.setValue(cine.cHoraf.getValue().toLocalTime());
            TimePInicio.setValue(cine.cHorai.getValue().toLocalTime());
            btnModifCorreo.setDisable(false);
            btnModifCorreo.setVisible(true);
            txtCorreo.setVisible(true);
            btnCambiarCorreo.setDisable(true);
            btnCambiarCorreo.setVisible(false);

        } else {
            nuevo = true;
            habilitacorreo();
            btnCambiarCorreo.setDisable(true);
            btnCambiarCorreo.setVisible(false);
            btnModifCorreo.setDisable(true);
            btnModifCorreo.setVisible(false);
            txtCorreo.setVisible(false);
        }

    }

    public void CargarModificaciones() {
        boolean v = false;
        if (nuevo) {
            this.cine = new CineDto();
            v = validaCorreo();
            if (!v) {
                String correo = txtIniCorreo.getText() + cmbEmpre.getSelectionModel().getSelectedItem() + cmbDominio.getSelectionModel().getSelectedItem();
                this.cine.correo.setValue(correo);
                cine.setcNombre(txtNomCine.getText());
                cine.setcTelefono(Integer.valueOf(txtTelefono.getText()));
                LocalDate fecha = LocalDate.of(2018, 1, 1);
                LocalDateTime horaFin = LocalDateTime.of(fecha, TimePFin.getValue());
                cine.cHoraf.setValue(horaFin);
                LocalDateTime horaini = LocalDateTime.of(fecha, TimePInicio.getValue());
                cine.cHorai.setValue(horaini);
            }
        } else {
             
              this.cine.cNombre.setValue(txtNomCine.getText());
              this.cine.cTelefono.setValue(txtTelefono.getText());
              LocalDate fecha = LocalDate.of(2018, 1, 1);
            LocalDateTime horaFin = LocalDateTime.of(fecha, TimePFin.getValue());
             cine.cHoraf.setValue(horaFin);
            LocalDateTime horaini = LocalDateTime.of(fecha, TimePInicio.getValue());
            cine.cHorai.setValue(horaini);
        }

    }

    public void requeridos() {

        req.clear();
        req.addAll(Arrays.asList(txtNomCine, txtTelefono));

    }

    public boolean validar() {
        requeridos();
        Boolean req1 = false;
        for (JFXTextField x : this.req) {
            if (x.getText().isEmpty() || x.equals(" ")) {
                req1 = true;
            }
        }
        return req1;
    }

    public void habilitacorreo() {
        cmbDominio.setVisible(true);
        cmbDominio.setDisable(false);
        cmbEmpre.setVisible(true);
        cmbEmpre.setDisable(false);
        txtIniCorreo.setDisable(false);
        txtIniCorreo.setVisible(true);
        btnCambiarCorreo.setVisible(true);
        btnCambiarCorreo.setDisable(false);
        this.empresa = FXCollections.observableArrayList();
        this.dominio = FXCollections.observableArrayList();
        this.empresa.addAll("@hotmail", "@gmail", "@outlook");
        this.dominio.addAll(".es", ".com");

        this.cmbEmpre.setItems((ObservableList) this.empresa);
        this.cmbDominio.setItems((ObservableList) this.dominio);
    }

    public boolean validaCorreo() {
        boolean incorrecto = false;
        if ("".equals(txtIniCorreo.getText()) || txtIniCorreo.getText().isEmpty()) {
            incorrecto = true;
        }
        if (cmbDominio.getSelectionModel().getSelectedItem() == null || cmbEmpre.getSelectionModel().getSelectedItem() == null) {
            incorrecto = true;
        }
        if (incorrecto) {
            Mensaje.showModal(Alert.AlertType.ERROR, "Correo", getStage(), "Correo ingresado incorrectamente");
        }
        return incorrecto;
    }

    @FXML
    private void btnGuardar(MouseEvent event) {

        guardar();

    }

    @FXML
    private void btnModificar(MouseEvent event) {
        txtNomCine.setDisable(false);
        txtTelefono.setDisable(false);
        TimePInicio.setDisable(false);
        TimePFin.setDisable(false);
        btnGuardar.setVisible(true);
        btnGuardar.setDisable(false);
        
    }

    @FXML
    private void btnModfCorreo(MouseEvent event) {
        habilitacorreo();
    }

    @FXML
    private void btnCambiarCorreo(MouseEvent event) {
        boolean v = validaCorreo();
        if (!v) {
            try {
                this.cine = (CineDto) AppContext.getInstance().get("Cine");
                String correo = txtIniCorreo.getText() + cmbEmpre.getSelectionModel().getSelectedItem() + cmbDominio.getSelectionModel().getSelectedItem();
                this.cine.correo.setValue(correo);
                CineService cineService = new CineService();
                Respuesta res = cineService.guardarCine(cine);
                if (!res.getEstado()) {
                    Mensaje.showModal(Alert.AlertType.ERROR, "Modificar", getStage(), res.getMensaje());
                } else {
                   
                    AppContext.getInstance().set("Cine", (CineDto) res.getResultado("Cine"));
                    Mensaje.showModal(Alert.AlertType.INFORMATION, "Modificar", getStage(), "Modificación exitosa.");
                    desactivarComponentes();
                    CargarDatos();
                }

            } catch (Exception ex) {
                Logger.getLogger(MantCineController.class.getName()).log(Level.SEVERE, "Error modificando los datos del cine.", ex);
            }
        }

    }

    private void desactivarComponentes() {
        txtNomCine.setDisable(true);
        txtTelefono.setDisable(true);
        TimePInicio.setDisable(true);
        TimePFin.setDisable(true);
        txtIniCorreo.setDisable(true);
        txtIniCorreo.setVisible(false);
        cmbDominio.setVisible(false);
        cmbDominio.setDisable(true);
        cmbEmpre.setVisible(false);
        cmbEmpre.setDisable(true);
        btnGuardar.setVisible(false);
        btnGuardar.setDisable(true);
    }

    @FXML
    private void formato(KeyEvent e) {
        if ((e.getSource().equals(this.txtIniCorreo) && e.getCharacter().equalsIgnoreCase("@")) || this.txtIniCorreo.getText().length() > 20) {
            e.consume();
        }
    }

    private void guardar() {
        boolean validar = validar();
        if (validar || TimePFin.getValue() == null || TimePInicio.getValue() == null) {
            Mensaje.showModal(Alert.AlertType.ERROR, "Campos necesarios ", getStage(), "Debe ingresar todos los datos solicitados.");
        } else {
            if (TimePInicio.getValue().isAfter(TimePFin.getValue())) {
                Mensaje.showModal(Alert.AlertType.ERROR, "Horario", getStage(), "La hora de inicio no puede ser después de la hora de cierre.");
            } else {
                CargarModificaciones();
                try {
                    CineService cineService = new CineService();
                    Respuesta res = cineService.guardarCine(cine);
                    if (!res.getEstado()) {
                        Mensaje.showModal(Alert.AlertType.ERROR, "Modificar", getStage(), res.getMensaje());
                    } else {
//                        cine = (CineDto) res.getResultado("Cine");
//                        AppContext.getInstance().set("Cine", (CineDto) res.getResultado("Cine"));
                        Mensaje.showModal(Alert.AlertType.INFORMATION, "Modificar", getStage(), "Modificación exitosa.");
                        desactivarComponentes();
                        CargarDatos();
                    }

                } catch (Exception ex) {
                    Logger.getLogger(MantCineController.class.getName()).log(Level.SEVERE, "Error modificando los datos del cine.", ex);
                }
            }
        }
    }

    @FXML
    private void cambioFondo(ActionEvent event) {
        FileChooser filechose = new FileChooser();
        filechose.setTitle("Buscar Imagen");
        filechose.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("*.png", "*.jpg*")
        );
        File file = filechose.showOpenDialog(getStage());
        if (file!= null && !file.getAbsolutePath().equals("")) {
            Image im1 = new Image("file:" + file.getAbsolutePath());
            f.guardarImagen(im1);
            imgPelicula.setImage(im1);
        }
    }

}
