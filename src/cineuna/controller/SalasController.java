/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.controller;

import cineuna.model.SalaDto;
import cineuna.service.SalaService;
import cineuna.util.AppContext;
import cineuna.util.ConverArchivo;
import cineuna.util.FlowController;
import cineuna.util.Mensaje;
import cineuna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXTextField;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

/**
 * FXML Controller class
 *
 * @author liedu
 */
public class SalasController extends Controller implements Initializable {

    @FXML
    private JFXTextField txtNombreSala;
    @FXML
    private JFXButton btnCrear;
    @FXML
    private VBox vboxSalas;
    @FXML
    private JFXCheckBox cbxEliminar;
    @FXML
    private ImageView imgFondo;
    @FXML
    private JFXButton cargarImagen;

    private Boolean eliminar;
    private List<SalaDto> salas;
    private ObservableList<JFXButton> botones;
    private String idioma;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO   
    }

    @Override
    public void initialize() {
        iniciarComponentes();
    }

    public void baseDatos() {
        SalaService service = new SalaService();
        Respuesta resp = new Respuesta();
        resp = service.getSalas();
        if (resp.getEstado()) {
            salas = (List<SalaDto>) resp.getResultado("Salas");
            for (int i = 0; i < salas.size(); i++) {
                cargarTexto(salas.get(i));
            }
            System.out.println("Salas recuperadas con exito");
        } else {
            System.out.println("Error alrecuperar las salas ");
        }

    }

    public void iniciarComponentes() {
        this.idioma = (String) AppContext.getInstance().get("idioma");
        this.eliminar = false;
        this.txtNombreSala.setLabelFloat(true);
        this.salas = new ArrayList();
        this.botones = FXCollections.observableArrayList();
        this.vboxSalas.getChildren().clear();
        this.vboxSalas.getChildren().addAll(botones);
        baseDatos();
    }
//680x100

    @FXML
    private void crearSala(ActionEvent event) {
        if (txtNombreSala.getText() != null && !txtNombreSala.getText().equals("") && imgFondo.getImage() != null) {
            SalaDto sal = new SalaDto();
            sal.setSaNombre(txtNombreSala.getText());
            sal.setSaEstado("I");
            sal.setSaFonImagen(ConverArchivo.imageToString(imgFondo.getImage(), "png"));
            SalaService service = new SalaService();
            Respuesta resp = service.guardar(sal);
            if (resp.getEstado()) {
                sal = (SalaDto) resp.getResultado("Sala");
                salas.add(sal);
                cargarTexto(sal);
                txtNombreSala.clear();
                imgFondo.setImage(null);
            } else {
                if (this.idioma.equalsIgnoreCase("ES")) {
                    Mensaje.show(Alert.AlertType.ERROR, "Error creando sala", resp.getMensaje() + "..");
                } else {
                    Mensaje.show(Alert.AlertType.ERROR, "Error creating room", resp.getMensaje() + "..");
                }
            }
        } else {
            if (txtNombreSala.getText() == null || txtNombreSala.getText().equals("")) {
                if (this.idioma.equalsIgnoreCase("ES")) {
                    Mensaje.show(Alert.AlertType.ERROR, "Error creando sala", "Debe ingresar un nombre para la sala.");
                } else {
                    Mensaje.show(Alert.AlertType.ERROR, "Error creating room", "You must enter a name for the room.");
                }
            } else {
                if (this.idioma.equalsIgnoreCase("ES")) {
                    Mensaje.show(Alert.AlertType.ERROR, "Error creando sala", "Debe selecionar una imagen.");
                } else {
                    Mensaje.show(Alert.AlertType.ERROR, "Error creating room", "you need to select an image.");
                }

            }
        }
    }

    /**
     * Carga los datos visibles al boton
     */
    public void cargarTexto(SalaDto sal) {
        JFXButton btn = new JFXButton();
        ImageView imgAc = new ImageView(new Image("cineuna/resources/Disponible.png"));
        ImageView imgIn = new ImageView(new Image("cineuna/resources/noDisponible.png"));
        if (sal.getSaEstado().equals("A")) {
            btn.setGraphic(imgAc);
        } else {
            btn.setGraphic(imgIn);
        }
        btn.setId(sal.getSaId().toString());
        btn.setText(sal.getSaNombre() + "\n" + estado(sal));
        btn.setMinSize(680, 90);
        btn.setOnMouseClicked(clic);
        botones.add(btn);
        vboxSalas.getChildren().clear();
        vboxSalas.getChildren().addAll(botones);
    }

    /**
     * Trasforma el estado para el visual
     *
     * @param sal
     * @return
     */
    public String estado(SalaDto sal) {
        if (sal.getSaEstado().equals("A")) {
            return "Activa";
        } else {
            return "Inactiva";
        }
    }

    EventHandler<MouseEvent> clic = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            JFXButton btn = (JFXButton) event.getSource();
            if (eliminar) {
//                salas.remove(a);
//                vboxSalas.getChildren().clear();
//                vboxSalas.getChildren().addAll(salas);
            } else {
                for (SalaDto sala : salas) {
                    if (sala.getSaId().toString().equals(btn.getId())) {
                        AppContext.getInstance().set("Sala", sala);
                        FlowController.getInstance().goView("EditorSala");
                    }

                }
            }

        }
    };

    @FXML
    private void eliminar(ActionEvent event) {
        eliminar = cbxEliminar.isSelected();
        if (cbxEliminar.isSelected()) {
            if (this.idioma.equalsIgnoreCase("ES")) {
                cbxEliminar.setText("Seleccione elemento a eliminar");
            } else {
                cbxEliminar.setText("Select item to be deleted");
            }
            btnCrear.setDisable(true);
            txtNombreSala.setDisable(true);
        } else {
            if (this.idioma.equalsIgnoreCase("ES")) {
                cbxEliminar.setText("Eliminar");
            } else {
                cbxEliminar.setText("Delete");
            }
            btnCrear.setDisable(false);
            txtNombreSala.setDisable(false);
        }
    }

    @FXML
    private void cargarImagen(ActionEvent event) {
        if (Mensaje.showConfirmation("Alerta!!", getStage(), "La imagen que usted seleccione,\npodria afectar la apariencia de la interfaz.\n¿Desea continuar?")) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Buscar Imagen");
            // Agregar filtros para facilitar la busqueda
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("All Images", "*.*"),
                    new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                    new FileChooser.ExtensionFilter("PNG", "*.png")
            );
            // Obtener la imagen seleccionada
            File imgFile = fileChooser.showOpenDialog(getStage());
            // Mostar la imagen
            if (imgFile != null) {
                ImageView imgImagen = new ImageView("file:" + imgFile.getAbsolutePath());
                imgImagen.setFitHeight(80);
                imgImagen.setFitWidth(80);
                this.imgFondo.setImage(imgImagen.getImage());
            }
        }
    }

}//Fin clase
