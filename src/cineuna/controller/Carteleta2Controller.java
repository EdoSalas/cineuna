/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.controller;

import cineuna.model.PeliculaDto;
import cineuna.model.SalaDto;
import cineuna.service.PeliculaService;
import cineuna.util.AppContext;
import cineuna.util.ConverArchivo;
import cineuna.util.FlowController;
import cineuna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author Matami
 */
public class Carteleta2Controller extends Controller implements Initializable {

    @FXML
    private HBox hbProximos;
    @FXML
    private HBox hbenCarte;

    private List<PeliculaDto> peliculas;

    private List<JFXButton> botonPelicula = new ArrayList();
    
    private String idioma;
    
     private Boolean compra;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        idioma = (String) AppContext.getInstance().get("idioma");
        compra = (Boolean) AppContext.getInstance().get("EnCompra");
        cargarPeliculas();
    }

    @Override
    public void initialize() {
    }

    public void cargarPeliculas() {
        hbProximos.getChildren().clear();
        hbenCarte.getChildren().clear();
        PeliculaService peliculaService = new PeliculaService();
        Respuesta res = peliculaService.getPeliculasList();
        if (!res.getEstado()) {

        } else {
            peliculas = (List<PeliculaDto>) res.getResultado("Peliculas");
            acomodar();
            verificarFecha();
            for (PeliculaDto pe : this.peliculas) {
                if (!pe.getPeEstado().equalsIgnoreCase("I")) {
                    cargarTextoImagenPeli(pe);
                }
            }
        }

    }

    public void acomodar() {
        Collections.sort(peliculas, new Comparator<PeliculaDto>() {
            @Override
            public int compare(PeliculaDto u1, PeliculaDto u2) {
                return new Long(u1.getPeFestreno().toEpochDay()).compareTo(u2.getPeFestreno().toEpochDay());
            }
        });
    }
    public void verificarFecha(){
        LocalDate ld = LocalDate.now();
        peliculas.forEach((x)->{
            if(x.getPeFestreno().toEpochDay()<=ld.toEpochDay()){
                x.setPeEstado("E");
            }
        });
    }

    public void cargarTextoImagenPeli(PeliculaDto pelicula) {
        JFXButton btn = new JFXButton();
        ImageView imgAc = new ImageView(ConverArchivo.stringToImage(pelicula.getPeImagenS()));
        imgAc.setFitWidth(140);
        imgAc.setFitHeight(140);
        //cargar imagen
        btn.setGraphic(imgAc);
        btn.setMinSize(160, 160);
        //cargar elvento
        btn.setOnMouseClicked(clic);
        //ajustar el texto e imagen

        btn.setStyle("-fx-font-size: 15px;-fx-content-display: top;");  
        
        btn.setTooltip(new Tooltip("Click para ver mas infomacion  / Click to see more information"));
        btn.setId(pelicula.getPeId().toString());
        botonPelicula.add(btn);
        String nombre = "";
        if(idioma.equalsIgnoreCase("Es")){
            nombre = pelicula.getPeNombreEs();
        }else{
            nombre = pelicula.getPeNombreIn();
        }
        if (pelicula.getPeEstado().equalsIgnoreCase("D")) {
           
            btn.setText(nombre+ "\n" + pelicula.getPeFestreno().toString());
            hbProximos.getChildren().add(btn);
        } else {
           
            btn.setText(nombre + "\n" + pelicula.getPeNombreIn());
            hbenCarte.getChildren().add(btn);
        }
    }
    EventHandler<MouseEvent> clic = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            JFXButton btn = (JFXButton) event.getSource();

            for (PeliculaDto peli : peliculas) {
                if (peli.getPeId().toString().equals(btn.getId())) {
                    AppContext.getInstance().set("PeliculaCargada", peli);
                    FlowController.getInstance().goView("Pelicula");//falta cambiar aqui

                }

            }
        }
    };
}
