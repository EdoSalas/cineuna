/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.controller;

import cineuna.model.PeliculaDto;
import cineuna.util.AppContext;
import cineuna.util.ConverArchivo;
import cineuna.util.FlowController;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.web.WebView;

/**
 * FXML Controller class
 *
 * @author Eduardo Salas Cerdas
 */
public class PeliculaController extends Controller implements Initializable {

    @FXML
    private ImageView imgPelicula;
    @FXML
    private JFXTextField txtNombre;
    @FXML
    private JFXTextArea txaGenero;
    @FXML
    private JFXTextArea txaDescripcion;
    @FXML
    private WebView web;
    @FXML
    private JFXButton btnComprar;
    
    private PeliculaDto p;
    
    private Boolean compra;
     
    private PeliculaDto peli;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
          compra = (Boolean) AppContext.getInstance().get("EnCompra");
    }    

    @Override
    public void initialize() {
        inicializa();
    }
    
    public void inicializa(){
        AppContext.getInstance().set("web", this.web);
        p = (PeliculaDto) AppContext.getInstance().get("PeliculaCargada");
        if(p != null){
            //this.imgPelicula.setImage(new ImageView(ConverArchivo.stringToImage(p.getPeImagenS())).getImage());
            if(((String) AppContext.getInstance().get("idioma")).equalsIgnoreCase("ES")){
                this.txtNombre.setText(p.getPeNombreEs());
                Integer arroba = p.getPeGenero().indexOf("@");
                this.txaGenero.setText(p.getPeGenero().substring(0, arroba));
                this.txaDescripcion.setText(p.getPeResennaEs());
                this.web.getEngine().load(p.getPeLinkEs());
            }else{
                this.txtNombre.setText(p.getPeNombreIn());
                Integer arroba = p.getPeGenero().indexOf("@");
                this.txaGenero.setText(p.getPeGenero().substring(arroba, p.getPeGenero().length()));
                this.txaDescripcion.setText(p.getPeResennaIn());
                this.web.getEngine().load(p.getPeLinkIn());
            }
        }
    }

    @FXML
    private void comprar(ActionEvent event) {
        AppContext.getInstance().set("PeliTandas", p);
        FlowController.getInstance().goView("VerTandas");
    }
    
}
