/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.controller;

import cineuna.model.UsuarioDto;
import cineuna.service.UsuarioService;
import cineuna.util.BindingUtils;
import cineuna.util.EnvioCorreo;
import cineuna.util.FlowController;
import cineuna.util.Formato;
import cineuna.util.Mensaje;
import cineuna.util.Respuesta;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Eduardo Salas Cerdas
 */
public class RegistroController extends Controller implements Initializable {

    @FXML
    private Label lblTitulo;
    @FXML
    private JFXTextField txtNombre;
    @FXML
    private JFXTextField txtApellido;
    @FXML
    private JFXRadioButton rbMasculino;
    @FXML
    private ToggleGroup Genero;
    @FXML
    private JFXRadioButton rbFemenino;
    @FXML
    private JFXTextField txtUsuario;
    @FXML
    private JFXPasswordField passClave;
    @FXML
    private JFXTextField txtClave;
    @FXML
    private JFXTextField txtCorreo;
    @FXML
    private Label lblAtras;
    @FXML
    private HBox hbClave;
    @FXML
    private FontAwesomeIconView fontVista;
    @FXML
    private Label lblRegistro;
    @FXML
    private JFXTextField txtCedula;
    @FXML
    private JFXTextField txtTelefono;
    @FXML
    private JFXComboBox<String> cbEmpresa;
    @FXML
    private JFXComboBox<String> cbDominio;
    @FXML
    private JFXTextField txtCorreoBind;

    private UsuarioDto usuarioDto;
    private ArrayList<JFXTextField> req = new ArrayList();
    private ObservableList<String> empresa;
    private ObservableList<String> dominio;
    private String nEmpresa;
    private String nDominio;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        iniciarComponentes();
    }

    @Override
    public void initialize() {

    }

    public void iniciarComponentes() {
        //Formato
        txtApellido.textFormatterProperty().set(Formato.getInstance().letrasFormat(40));
        txtCedula.textFormatterProperty().set(Formato.getInstance().cedulaFormat(20));
        txtClave.textFormatterProperty().set(Formato.getInstance().cedulaFormat(20));
        txtNombre.textFormatterProperty().set(Formato.getInstance().letrasFormat(40));
        txtTelefono.textFormatterProperty().set(Formato.getInstance().integerFormat());
        txtUsuario.textFormatterProperty().set(Formato.getInstance().maxLengthFormat(20));
        txtCorreo.textFormatterProperty().set(Formato.getInstance().maxLengthFormat(40));

        this.txtNombre.setLabelFloat(true);
        this.txtApellido.setLabelFloat(true);
        this.rbMasculino.setUserData("M");
        this.rbFemenino.setUserData("F");
        this.txtUsuario.setLabelFloat(true);
        this.passClave.setLabelFloat(true);
        this.txtClave.setLabelFloat(true);
        this.hbClave.getChildren().remove(this.txtClave);
        this.txtCorreo.setLabelFloat(true);
        this.txtCedula.setLabelFloat(true);
        this.txtTelefono.setLabelFloat(true);
        this.empresa = FXCollections.observableArrayList();
        this.dominio = FXCollections.observableArrayList();
        this.empresa.addAll("@hotmail", "@gmail", "@outlook");
        this.dominio.addAll(".es", ".com");
        this.nEmpresa = "";
        this.nDominio = "";
        this.cbEmpresa.setItems((ObservableList) this.empresa);
        this.cbDominio.setItems((ObservableList) this.dominio);
        this.txtCorreoBind.setVisible(false);

        atras();
        registrarse();
        usuarioDto = new UsuarioDto();
        requeridos();
        nuevoUsuarioDto();
        icon();
    }

    public void requeridos() {
        req.clear();
        req.addAll(Arrays.asList(txtCedula, txtApellido, txtCorreo, txtNombre, txtTelefono, txtUsuario));

    }

    public void bindUsuarioDto() {
        txtApellido.textProperty().bindBidirectional(usuarioDto.usApellido);
        txtCorreoBind.textProperty().bindBidirectional(usuarioDto.usCorreo);
        txtNombre.textProperty().bindBidirectional(usuarioDto.usNombre);
        txtUsuario.textProperty().bindBidirectional(usuarioDto.usUsuario);
        txtTelefono.textProperty().bindBidirectional(usuarioDto.usTelefono);
        txtCedula.textProperty().bindBidirectional(usuarioDto.usCedula);
        BindingUtils.bindToggleGroupToProperty(Genero, usuarioDto.usGenero);

    }

    public void unbindUsurarioDto() {
        txtApellido.textProperty().unbindBidirectional(usuarioDto.usApellido);
        txtCorreoBind.textProperty().unbindBidirectional(usuarioDto.usCorreo);
        txtNombre.textProperty().unbindBidirectional(usuarioDto.usNombre);
        txtUsuario.textProperty().unbindBidirectional(usuarioDto.usUsuario);
        txtTelefono.textProperty().unbindBidirectional(usuarioDto.usTelefono);
        txtCedula.textProperty().unbindBidirectional(usuarioDto.usCedula);
        BindingUtils.unbindToggleGroupToProperty(Genero, usuarioDto.usGenero);
    }

    public void nuevoUsuarioDto() {
        unbindUsurarioDto();
        usuarioDto = new UsuarioDto();
        bindUsuarioDto();
        cargarClave();
    }

    public void icon() {
        this.fontVista.setOnMouseClicked((event) -> {
            if (estado()) {

                this.fontVista.setGlyphName("EYE_SLASH");
                this.hbClave.getChildren().add(this.txtClave);
                this.txtClave.setText(this.passClave.getText());
                this.hbClave.getChildren().remove(this.passClave);

            } else {

                this.fontVista.setGlyphName("EYE");
                this.hbClave.getChildren().add(this.passClave);
                this.passClave.setText(this.txtClave.getText());
                this.hbClave.getChildren().remove(this.txtClave);

            }
        });
    }

    public void cargarClave() {
        if (!estado()) {
            txtClave.textProperty().unbindBidirectional(usuarioDto.usClave);
            txtClave.textProperty().bindBidirectional(usuarioDto.usClave);
        } else {
            passClave.textProperty().unbindBidirectional(usuarioDto.usClave);
            passClave.textProperty().bindBidirectional(usuarioDto.usClave);
        }
    }

    public Boolean estado() {
        return this.fontVista.getGlyphName().equalsIgnoreCase("EYE");
    }

    public void atras() {
        this.lblAtras.setOnMouseClicked((event) -> {
            FlowController.getInstance().delete("Registro");
            FlowController.getInstance().goViewInWindow("LogIng");
            ((Stage) this.lblAtras.getScene().getWindow()).close();
            event.consume();
        });

    }

    public void registrarse() {
        lblRegistro.setOnMouseClicked((Event) -> {
            registrarUsuarioDto();

        });
    }

    public void registrarUsuarioDto() {
        try {
            String invalidos = requeridoCampos();
            if (!invalidos.isEmpty()) {
                Mensaje.showModal(Alert.AlertType.ERROR, "Campos necesarios ", getStage(), invalidos);
            } else {
                UsuarioService usuarioService = new UsuarioService();
                if(!usuarioService.validarUsuario(txtUsuario.getText()).getEstado()){
                Respuesta res = usuarioService.guardar(usuarioDto);
                if (!res.getEstado()) {
                    Mensaje.showModal(Alert.AlertType.ERROR, "Registro", getStage(), res.getMensaje());
                } else {
                    unbindUsurarioDto();
                    usuarioDto = (UsuarioDto) res.getResultado("Usuario");
                    bindUsuarioDto();
                    Mensaje.showModal(Alert.AlertType.INFORMATION, "Registro", getStage(), "Registro exitoso, favor confirmar correo.");
                    enviarCorreo(usuarioDto.getUsCorreo(), usuarioDto.getUsCodigoActivacion());

                }
            }else{
                    Mensaje.show(Alert.AlertType.INFORMATION, "", FlowController.getInstance().getLanguage().getString("msExisUsuario"));
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(RegistroController.class.getName()).log(Level.SEVERE, " Error al registrar el usuario. ", ex);
            Mensaje.showModal(Alert.AlertType.ERROR, "Regitro", getStage(), "Lo sentimos, ocurrio un error al registrar el usuario. ");
        }
    }

    /**
     * verifica la respusta al enviar el correo
     *
     * @param correo
     * @param id
     */
    public void enviarCorreo(String correo, String id) throws IOException {
        if (EnvioCorreo.getInstance().enviarActivacion(correo, id).getEstado()) {
            if (Mensaje.showConfirmation("Eviado", getStage(), "Envio correcto del correo de activacion")) {;
                FlowController.getInstance().delete("Registro");
                FlowController.getInstance().goViewInWindow("LogIng");
                ((Stage) this.lblAtras.getScene().getWindow()).close();
            }
        } else {
            Mensaje.show(Alert.AlertType.ERROR, "Eviado", "Lo sentimos en este momento no podemos enviar el correo de activacion");
        }
    }

    public String requeridoCampos() {
        String requerido = "";
        Boolean req = false;
        for (JFXTextField x : this.req) {
            if (x.getText() == null || x.equals("")) {
                requerido = ", " + x.getPromptText();
                req = true;
            }
        }

        if (estado()) {
            if (txtClave.getText() == null || txtClave.equals("")) {
                requerido = ", " + txtClave.getPromptText();
                req = true;
            }
        } else {
            if (passClave.getText() == null || passClave.equals("")) {
                requerido = ", " + passClave.getPromptText();
                req = true;
            }
        }
        if (req) {
            return requerido;
        } else {
            return "";
        }
    }

    @FXML
    private void formato(KeyEvent e) {
        if (cbEmpresa.getSelectionModel().getSelectedItem() == null) {
            nEmpresa = "";
        } else {
            nEmpresa = cbEmpresa.getSelectionModel().getSelectedItem();
        }
        if (cbDominio.getSelectionModel().getSelectedItem() == null) {
            nDominio = "";
        } else {
            nDominio = cbDominio.getSelectionModel().getSelectedItem();
        }
        if ((e.getSource().equals(this.txtCorreo) && e.getCharacter().equalsIgnoreCase("@")) || this.txtCorreo.getText().length() > 20) {
            e.consume();
        } else {
            this.txtCorreoBind.setText(this.txtCorreo.getText() + e.getCharacter() + nEmpresa + nDominio);
        }
    }

    @FXML
    private void comboEmpresa(ActionEvent event) {
        if (cbEmpresa.getSelectionModel().getSelectedItem() == null) {
            nEmpresa = "";
        } else {
            nEmpresa = cbEmpresa.getSelectionModel().getSelectedItem();
        }
        if (cbDominio.getSelectionModel().getSelectedItem() == null) {
            nDominio = "";
        } else {
            nDominio = cbDominio.getSelectionModel().getSelectedItem();
        }
        this.txtCorreoBind.setText(this.txtCorreo.getText() + nEmpresa + nDominio);
    }

    @FXML
    private void comboDominio(ActionEvent event) {
        if (cbEmpresa.getSelectionModel().getSelectedItem() == null) {
            nEmpresa = "";
        } else {
            nEmpresa = cbEmpresa.getSelectionModel().getSelectedItem();
        }
        if (cbDominio.getSelectionModel().getSelectedItem() == null) {
            nDominio = "";
        } else {
            nDominio = cbDominio.getSelectionModel().getSelectedItem();
        }
        this.txtCorreoBind.setText(this.txtCorreo.getText() + nEmpresa + nDominio);
    }
}
