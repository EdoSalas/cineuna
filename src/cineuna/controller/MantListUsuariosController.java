/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.controller;

import cineuna.model.UsuarioDto;
import cineuna.service.UsuarioService;
import cineuna.util.AppContext;
import cineuna.util.FlowController;
import cineuna.util.Mensaje;
import cineuna.util.Respuesta;

import com.jfoenix.controls.JFXCheckBox;
import java.net.URL;

import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Emily
 */
public class MantListUsuariosController extends Controller implements Initializable {

    @FXML
    private TabPane tabMantUsuario;
    @FXML
    private TableView<UsuarioDto> tbvUsuarios;
    @FXML
    private TableColumn<UsuarioDto, String> ColID;
    @FXML
    private TableColumn<UsuarioDto, String> ColNomUsu;
    @FXML
    private TableColumn<UsuarioDto, String> ColUsuAdm;
    @FXML
    private JFXCheckBox ckbAdm;
    @FXML
    private JFXCheckBox ckbActivo;
    @FXML
    private Button btnGuardarCambio;
    @FXML
    private TableColumn<UsuarioDto, String> colApellido;
    UsuarioDto usuario;
    @FXML
    private Tab tapUsu;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        initialize();

    }

    @FXML
    private void tbview(MouseEvent event) {
        if (tbvUsuarios.getSelectionModel().getSelectedItem() != null) {
            if ("A".equals(tbvUsuarios.getSelectionModel().getSelectedItem().getUsAdministrador())) {
                ckbAdm.setSelected(true);
            } else {
                ckbAdm.setSelected(false);
            }
            ckbActivo.setSelected(true);
            btnGuardarCambio.setDisable(false);
        }
    }

    @FXML
    private void guardar(MouseEvent event) {
        if (tbvUsuarios.getSelectionModel().getSelectedItem() == null) {
            Mensaje.showModal(Alert.AlertType.ERROR, "", getStage(), FlowController.getInstance().getLanguage().getString("msSinCambio"));
        } else {
            if (((ckbAdm.isSelected() && "A".equals(tbvUsuarios.getSelectionModel().getSelectedItem().getUsAdministrador()))
                    || (!ckbAdm.isSelected() && "I".equals(tbvUsuarios.getSelectionModel().getSelectedItem().getUsAdministrador()))) && ckbActivo.isSelected()) {
                Mensaje.showModal(Alert.AlertType.ERROR, "", getStage(), FlowController.getInstance().getLanguage().getString("msSinCambio"));
            } else {
                UsuarioDto usuario = tbvUsuarios.getSelectionModel().getSelectedItem();
                if (ckbAdm.isSelected() && !"A".equals(tbvUsuarios.getSelectionModel().getSelectedItem().getUsAdministrador())) {
                    usuario.setUsAdministrador("A");
                }
                if (!ckbAdm.isSelected() && "A".equals(tbvUsuarios.getSelectionModel().getSelectedItem().getUsAdministrador())) {
                    usuario.setUsAdministrador("N");
                }
                if (!ckbActivo.isSelected()) {
                    usuario.setUsActivo("I");
                }
                guardar(usuario);

                ckbActivo.setSelected(false);
                ckbAdm.setSelected(false);
            }
        }

    }

    public void cargarUsuarios(UsuarioDto usuario) {
        List<UsuarioDto> ListUsuarios;
        UsuarioService usuarioService = new UsuarioService();
        Respuesta res = usuarioService.getUsuariosActivos();
        AppContext.getInstance().set("Usuarios", (List<UsuarioDto>) res.getResultado("Usuarios"));
        ListUsuarios = (List<UsuarioDto>) res.getResultado("Usuarios");

        for (int i = 0; i < ListUsuarios.size(); i++) {
            if (ListUsuarios.get(i).getUsId().equals(usuario.getUsId())) {
                ListUsuarios.remove(i);
            }
        }

        ObservableList<UsuarioDto> ObList = FXCollections.observableArrayList(ListUsuarios);
        tbvUsuarios.getItems().clear();
        tbvUsuarios.getItems().addAll(ObList);
    }

    public void guardar(UsuarioDto usuario) {
        try {
            UsuarioService usuarioService = new UsuarioService();
            Respuesta res = usuarioService.guardar(usuario);
            if (!res.getEstado()) {
                Mensaje.showModal(Alert.AlertType.ERROR, FlowController.getInstance().getLanguage().getString("msModificar"), getStage(), res.getMensaje());
            } else {
                //usuario = (UsuarioDto) res.getResultado("Usuario");
                Mensaje.showModal(Alert.AlertType.INFORMATION, FlowController.getInstance().getLanguage().getString("msModificar"), getStage(), FlowController.getInstance().getLanguage().getString("msModExi"));
                cargarUsuarios(this.usuario);
            }
        } catch (Exception ex) {
            Logger.getLogger(MantUsuarioController.class.getName()).log(Level.SEVERE, FlowController.getInstance().getLanguage().getString("msModErr"), ex);
        }

    }

    @FXML
    private void clickAdm(MouseEvent event) {
        if (tbvUsuarios.getSelectionModel().getSelectedItem() == null) {
            ckbAdm.setSelected(false);
        }
    }

    @FXML
    private void clickAct(MouseEvent event) {
        if (tbvUsuarios.getSelectionModel().getSelectedItem() == null) {
            ckbActivo.setSelected(false);
        }
    }

    @Override
    public void initialize() {

        ColID.setCellValueFactory((param) -> new SimpleStringProperty(param.getValue().getUsCedula()));
        ColNomUsu.setCellValueFactory((param) -> new SimpleStringProperty(param.getValue().getUsNombre()));
        ColUsuAdm.setCellValueFactory((param) -> new SimpleStringProperty(param.getValue().getUsAdministrador()));
        colApellido.setCellValueFactory((param) -> new SimpleStringProperty(param.getValue().getUsApellido()));
        ColNomUsu.setText(FlowController.getInstance().getLanguage().getString("txt.Nombre"));
        colApellido.setText(FlowController.getInstance().getLanguage().getString("txt.Apellido"));
        ColUsuAdm.setText(FlowController.getInstance().getLanguage().getString("cl.Adm"));
        ckbActivo.setText(FlowController.getInstance().getLanguage().getString("cbk.Activo"));
        ckbAdm.setText(FlowController.getInstance().getLanguage().getString("cbk.Adm"));
        tapUsu.setText(FlowController.getInstance().getLanguage().getString("tb.Usuario"));
        btnGuardarCambio.setText(FlowController.getInstance().getLanguage().getString("btn.GuardarListUs"));
        btnGuardarCambio.setTooltip(new Tooltip("Guardar las modificaciones hechas en el usuario. \n Save the modifications made to the user."));
        ckbAdm.setTooltip(new Tooltip("Asigna o desasigna al usuario como un nuevo administrador. \n Assign or unassign the user as a new administrator."));
        ckbActivo.setTooltip(new Tooltip("Desactiva la cuenta de un usuario. \n Disable a user's account."));
        this.usuario = (UsuarioDto) AppContext.getInstance().get("Usuario");

        cargarUsuarios(usuario);
    }
}
