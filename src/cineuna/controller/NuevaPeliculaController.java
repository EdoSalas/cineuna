/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.controller;

import cineuna.model.PeliculaDto;
import cineuna.service.PeliculaService;
import cineuna.util.AppContext;
import cineuna.util.ConverArchivo;
import cineuna.util.FlowController;
import cineuna.util.Mensaje;
import cineuna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;

/**
 * FXML Controller class
 *
 * @author Eduardo Salas Cerdas
 */
public class NuevaPeliculaController extends Controller implements Initializable {

    @FXML
    private JFXCheckBox ckAccion;
    @FXML
    private JFXCheckBox ckAventura;
    @FXML
    private JFXCheckBox ckComedia;
    @FXML
    private JFXCheckBox ckDrama;
    @FXML
    private JFXCheckBox ckTerror;
    @FXML
    private JFXTextField txtNombreES;
    @FXML
    private JFXTextField txtNombreEN;
    @FXML
    private JFXCheckBox ckSuspenso;
    @FXML
    private JFXCheckBox ckMusical;
    @FXML
    private JFXCheckBox ckCienciaFiccion;
    @FXML
    private JFXCheckBox ckGuerra;
    @FXML
    private JFXCheckBox ckOeste;
    @FXML
    private JFXCheckBox ckInfantil;
    @FXML
    private JFXTextArea txaDescripcionES;
    @FXML
    private JFXTextArea txaDescripcionEN;
    @FXML
    private JFXTextField txtUrlES;
    @FXML
    private JFXTextField txtUrlEN;
    @FXML
    private ImageView imgPelicula;
    @FXML
    private JFXRadioButton rbDisponible;
    @FXML
    private ToggleGroup estado;
    @FXML
    private JFXRadioButton rbCartelera;
    @FXML
    private JFXRadioButton rbInactiva;
    @FXML
    private JFXDatePicker date;
    @FXML
    private WebView webES;
    @FXML
    private WebView webEN;

    private Image image;

    private PeliculaDto peliculaDto = new PeliculaDto();

    private String idioma;
    @FXML
    private JFXButton btnVerTriEs;
    @FXML
    private FontAwesomeIconView btnVerTriIn;

    private ResourceBundle idioma1 = FlowController.getInstance().getLanguage();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

    }

    @Override
    public void initialize() {
        inicializaComponentes();
        btnVerTriEs.setTooltip(new Tooltip("Visualizacion previa de video \n Preview Video"));

    }

    public void inicializaComponentes() {

        if (((PeliculaDto) AppContext.getInstance().get("PeliculaCargada")) != null) {
            PeliculaDto pdto = ((PeliculaDto) AppContext.getInstance().get("PeliculaCargada"));
            //this.imgPelicula.setImage(new ImageView(ConverArchivo.stringToImage(pdto.getPeImagenS())).getImage());
            this.txtNombreES.setText(pdto.getPeNombreEs());
            this.txtNombreEN.setText(pdto.getPeNombreIn());
            this.date.setValue(pdto.getPeFestreno());
            marcaChecks(pdto);
            this.txaDescripcionES.setText(pdto.getPeResennaEs());
            this.txaDescripcionEN.setText(pdto.getPeResennaIn());
            this.txtUrlES.setText(pdto.getPeLinkEs());
            this.webES.getEngine().load(this.txtUrlES.getText());
            this.txtUrlEN.setText(pdto.getPeLinkIn());
            this.webEN.getEngine().load(this.txtUrlEN.getText());
            if (pdto.getPeEstado().equalsIgnoreCase("D")) {
                this.rbDisponible.setSelected(true);
            } else if (pdto.getPeEstado().equalsIgnoreCase("E")) {
                this.rbCartelera.setSelected(true);
            } else if (pdto.getPeEstado().equalsIgnoreCase("I")) {
                this.rbInactiva.setSelected(true);
            }
        } else {
            this.image = new Image("cineuna/resources/upload.png");
            this.imgPelicula.setImage(image);
        }
        this.idioma = (String) AppContext.getInstance().get("idioma");
        click();
    }

    public void click() {
        this.imgPelicula.setOnMouseClicked((MouseEvent e) -> {
            FileChooser filechose = new FileChooser();
            filechose.setTitle("Buscar Imagen");
            filechose.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("*.png", "*.jpg*")
            );
            File file = filechose.showOpenDialog(getStage());
            if (file != null && !file.getAbsolutePath().equals("")) {
                imgPelicula.setImage(new Image("file:" + file.getAbsolutePath()));
                
            }
            e.consume();
        });
    }

    public void cargarDatos() throws IOException {
        validarLink(txtUrlEN);
        validarLink(txtUrlES);
        peliculaDto = new PeliculaDto(ConverArchivo.imageToString(imgPelicula.getImage(), "png"), txtNombreES.getText(), txtNombreEN.getText(),
                txaDescripcionES.getText(), txaDescripcionEN.getText(), txtUrlES.getText(), txtUrlEN.getText(), estado(), generoPelicula(),
                date.getValue());
    }

    @FXML
    private void guardar(ActionEvent event) throws IOException {
        if (this.imgPelicula.getImage().equals(image) || generoPelicula().equalsIgnoreCase("") || txaDescripcionES.getText().isEmpty() || txaDescripcionEN.getText().isEmpty()
                || txtNombreES.getText().isEmpty() || txtNombreEN.getText().isEmpty() || txtUrlES.getText().isEmpty() || txtUrlEN.getText().isEmpty() || date.getValue() == null) {
            Mensaje.showModal(Alert.AlertType.INFORMATION, idioma1.getString("msGuardarT"), getStage(), idioma1.getString("msGuardarM"));
        } else {
            try {
                if (((PeliculaDto) AppContext.getInstance().get("PeliculaCargada")) == null) {
                    //Guardar Nuevo
                    cargarDatos();
                    PeliculaService peliculaService = new PeliculaService();
                    Respuesta respuesta = peliculaService.guardar(peliculaDto);
                    if (respuesta.getEstado()) {
                        Mensaje.showModal(Alert.AlertType.INFORMATION, idioma1.getString("msPeli"), getStage(), respuesta.getMensaje());

                    } else {
                        Mensaje.showModal(Alert.AlertType.ERROR, idioma1.getString("msPeli2"), getStage(), respuesta.getMensaje());
                    }
                    FlowController.getInstance().delete("NuevaPelicula");
                    FlowController.getInstance().goView("AdminPeliculas");
                } else {
                    //Modificar existente
                    PeliculaService peliculaService = new PeliculaService();
                    Respuesta respuesta = peliculaService.guardar(cargaDatosModificada());
                    if (respuesta.getEstado()) {
                        Mensaje.showModal(Alert.AlertType.INFORMATION, idioma1.getString("msPeliActualizada"), getStage(), respuesta.getMensaje());
                    } else {
                        Mensaje.showModal(Alert.AlertType.ERROR, idioma1.getString("msPeliActualizada2"), getStage(), respuesta.getMensaje());

                    }
                    FlowController.getInstance().delete("NuevaPelicula");
                    FlowController.getInstance().goView("AdminPeliculas");
                }
                AppContext.getInstance().delete("PeliculaCargada");
            } catch (Exception ex) {
                Logger.getLogger(LogIngController.class.getName()).log(Level.SEVERE, "Error guardando la pelicula.", ex);
            }
        }
    }

    public PeliculaDto cargaDatosModificada() {
        PeliculaDto p = (PeliculaDto) AppContext.getInstance().get("PeliculaCargada");
        p.setPeImagenS(ConverArchivo.imageToString(imgPelicula.getImage(), "png"));
        p.setPeEstado(estado());
        p.setPeFestreno(date.getValue());
        p.setPeNombreEs(txtNombreES.getText());
        p.setPeNombreIn(txtNombreEN.getText());
        p.setPeResennaEs(txaDescripcionES.getText());
        p.setPeResennaIn(txaDescripcionEN.getText());
        p.setPeLinkEs(txtUrlES.getText());
        p.setPeLinkIn(txtUrlEN.getText());
        p.setPeGenero(generoPelicula());
        return p;
    }

    public String estado() {
        String estadoPelicula = "";
        if (rbDisponible.isSelected()) {
            estadoPelicula = "D";
        } else if (rbCartelera.isSelected()) {
            estadoPelicula = "E";
        } else if (rbInactiva.isSelected()) {
            estadoPelicula = "I";
        }
        return estadoPelicula;
    }

    public String generoPelicula() {
        String generoEs = "";
        String generoEn = "";
        if (ckAccion.isSelected()) {
            generoEs += "Acción / ";
            generoEn += "Action / ";
        }
        if (ckAventura.isSelected()) {
            generoEs += "Aventura / ";
            generoEn += "Adventure / ";
        }
        if (ckComedia.isSelected()) {
            generoEs += "Comedia / ";
            generoEn += "Comedy / ";
        }
        if (ckDrama.isSelected()) {
            generoEs += "Drama / ";
            generoEn += "Drama / ";
        }
        if (ckTerror.isSelected()) {
            generoEs += "Terror / ";
            generoEn += "Terror / ";
        }
        if (ckSuspenso.isSelected()) {
            generoEs += "Suspenso / ";
            generoEn += "Suspense / ";
        }
        if (ckMusical.isSelected()) {
            generoEs += "Musical / ";
            generoEn += "Musical / ";
        }
        if (ckCienciaFiccion.isSelected()) {
            generoEs += "Ciencia Ficción / ";
            generoEn += "Science fiction / ";
        }
        if (ckGuerra.isSelected()) {
            generoEs += "Guerra / ";
            generoEn += "War / ";
        }
        if (ckOeste.isSelected()) {
            generoEs += "Oeste / ";
            generoEn += "West / ";
        }
        if (ckInfantil.isSelected()) {
            generoEs += "Infantil / ";
            generoEn += "Childish / ";
        }
        return generoEs.substring(0, generoEs.length() - 3) + "@" + generoEn.substring(0, generoEn.length() - 3);
    }

    @FXML
    private void verTrailerES(ActionEvent event) {
        if (validarLink(this.txtUrlES)) {
            this.webES.getEngine().load(this.txtUrlES.getText());
        }
    }

    @FXML
    private void verTrailerEN(ActionEvent event) {
        if (validarLink(this.txtUrlEN)) {
            this.webEN.getEngine().load(this.txtUrlEN.getText());
        }
    }

    public boolean validarLink(JFXTextField link) {
        String linkAnterior = link.getText();
        link.setText(link.getText().replace("https://www.youtube.com/watch?v=", "https://www.youtube.com/embed/"));
        if (!linkAnterior.equalsIgnoreCase(link.getText()) || link.getText().equalsIgnoreCase("https://www.youtube.com/embed/")) {
            return true;
        } else {
            link.clear();
            Mensaje.showModal(Alert.AlertType.WARNING, " ", getStage(), idioma1.getString("msLink"));

            return false;
        }
    }

    public void marcaChecks(PeliculaDto p) {
        Integer arroba = p.getPeGenero().indexOf("@");
        String genero = p.getPeGenero().substring(0, arroba);
        if (genero.contains("Acción")) {
            this.ckAccion.setSelected(true);
        }
        if (genero.contains("Aventura")) {
            this.ckAventura.setSelected(true);
        }
        if (genero.contains("Comedia")) {
            this.ckComedia.setSelected(true);
        }
        if (genero.contains("Drama")) {
            this.ckDrama.setSelected(true);
        }
        if (genero.contains("Terror")) {
            this.ckTerror.setSelected(true);
        }
        if (genero.contains("Suspenso")) {
            this.ckSuspenso.setSelected(true);
        }
        if (genero.contains("Musical")) {
            this.ckMusical.setSelected(true);
        }
        if (genero.contains("Ciencia Ficción")) {
            this.ckCienciaFiccion.setSelected(true);
        }
        if (genero.contains("Guerra")) {
            this.ckGuerra.setSelected(true);
        }
        if (genero.contains("Oeste")) {
            this.ckOeste.setSelected(true);
        }
        if (genero.contains("Infantil")) {
            this.ckInfantil.setSelected(true);
        }
    }
}
