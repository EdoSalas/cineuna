/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.controller;

import cineuna.model.UsuarioDto;
import cineuna.service.CineService;
import cineuna.service.UsuarioService;
import cineuna.util.AppContext;
import cineuna.util.Clave;
import cineuna.util.EnvioCorreo;
import cineuna.util.FlowController;
import cineuna.util.Mensaje;
import cineuna.util.Respuesta;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Eduardo Salas Cerdas
 */
public class LogIngController extends Controller implements Initializable {

    @FXML
    private Label lblTitulo;
    @FXML
    private JFXTextField txtUsuario;
    @FXML
    private FontAwesomeIconView fontVista;
    @FXML
    private JFXPasswordField passClave;
    @FXML
    private JFXTextField txtClave;
    @FXML
    private Label lblIniciar;
    @FXML
    private Label lblRegistro;
    @FXML
    private HBox hbClave;
    @FXML
    private Label lbRecuperarContra;
    @FXML
    private Label lblLenguaje;

    private Image image;

    private ResourceBundle idioma = FlowController.getInstance().getLanguage();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        iniciarComponentes();
    }

    @Override
    public void initialize() {

    }

    public void iniciarComponentes() {
        this.txtUsuario.setLabelFloat(true);
        this.passClave.setLabelFloat(true);
        this.hbClave.getChildren().remove(this.txtClave);
        this.txtClave.setLabelFloat(true);
        click();
    }

    public void click() {
        lenguaje();
        icon();
        iniciar();
        registro();
        recuperar();
    }

    public void lenguaje() {
        this.lblLenguaje.setOnMouseClicked((MouseEvent e) -> {
            if (lblLenguaje.getText().equalsIgnoreCase("Lenguaje: ES")) {
                FlowController.getInstance().loadLanguage(ResourceBundle.getBundle("cineuna/resources/i18n/en"));
            } else {
                FlowController.getInstance().loadLanguage(ResourceBundle.getBundle("cineuna/resources/i18n/es"));
            }
            FlowController.getInstance().goViewInWindow("LogIng");
            ((Stage) this.lblIniciar.getScene().getWindow()).close();
            e.consume();
        });
    }

    public void icon() {
        this.fontVista.setOnMouseClicked((event) -> {
            if (estado()) {
                this.fontVista.setGlyphName("EYE_SLASH");
                this.hbClave.getChildren().add(this.txtClave);
                this.txtClave.setText(this.passClave.getText());
                this.hbClave.getChildren().remove(this.passClave);
            } else {
                this.fontVista.setGlyphName("EYE");
                this.hbClave.getChildren().add(this.passClave);
                this.passClave.setText(this.txtClave.getText());
                this.hbClave.getChildren().remove(this.txtClave);
            }
        });
    }

    public Boolean estado() {
        return this.fontVista.getGlyphName().equalsIgnoreCase("EYE");
    }

    public void iniciar() {
        this.lblIniciar.setOnMouseClicked((event) -> {
            try {
                String idioma = this.lblLenguaje.getText().substring(10, lblLenguaje.getText().length()).trim();
                if (txtUsuario.getText() == null || txtUsuario.getText().isEmpty()) {
                    Mensaje.showModal(Alert.AlertType.ERROR, FlowController.getInstance().getLanguage().getString("ms.usuarioVacioTitulo"), (Stage) lblIniciar.getScene().getWindow(), FlowController.getInstance().getLanguage().getString("ms.usuarioVacio"));
                } else if (passClave.getText() == null || passClave.getText().isEmpty()) {
                    Mensaje.showModal(Alert.AlertType.ERROR, FlowController.getInstance().getLanguage().getString("ms.claveTituloV"), (Stage) lblIniciar.getScene().getWindow(), FlowController.getInstance().getLanguage().getString("ms.claveV"));
                } else {
                    UsuarioService usuarioService = new UsuarioService();
                    Respuesta respuesta = usuarioService.getUsuario(txtUsuario.getText(), passClave.getText());
                    if (respuesta.getEstado()) {
                        AppContext.getInstance().set("Usuario", (UsuarioDto) respuesta.getResultado("Usuario"));
                        AppContext.getInstance().set("idioma", idioma);
                        UsuarioDto usuario = (UsuarioDto) AppContext.getInstance().get("Usuario");
                        if (usuario.getUsRecuperarClave().equals("S")) {
                            FlowController.getInstance().delete("LogIng");
                            FlowController.getInstance().goViewInWindow("RecuperarContrasena");
                            ((Stage) this.lblIniciar.getScene().getWindow()).close();
                        } else {

                            if (((UsuarioDto) respuesta.getResultado("Usuario")).getUsActivo().equals("A")) {
                                if (((UsuarioDto) respuesta.getResultado("Usuario")).getUsAdministrador().equals("A")) {
                                    AppContext.getInstance().set("tipoUsuario", "Administrador");
                                } else {
                                    AppContext.getInstance().set("tipoUsuario", "Cliente");
                                }
                                AppContext.getInstance().set("stageLogIng", (Stage) lblIniciar.getScene().getWindow());
                                FlowController.getInstance().goMain();
                                ((Stage) lblIniciar.getScene().getWindow()).close();
                                FlowController.getInstance().delete("LogIng");
                            }else{
                                Mensaje.show(Alert.AlertType.INFORMATION,"", this.idioma.getString("msNoAtivo"));
                            }
                        }
                    } else {
                        Mensaje.showModal(Alert.AlertType.ERROR, "Ingreso", getStage(), respuesta.getMensaje());
                    }
                }
            } catch (Exception ex) {
                Logger.getLogger(LogIngController.class.getName()).log(Level.SEVERE, "Error ingresando.", ex);
            }
        });
    }

    public void registro() {
        this.lblRegistro.setOnMouseClicked((event) -> {

            FlowController.getInstance().delete("LogIng");
            FlowController.getInstance().goViewInWindow("Registro");
            ((Stage) this.lblIniciar.getScene().getWindow()).close();
            event.consume();

        });
    }

    public void recuperar() {
        this.lbRecuperarContra.setOnMouseClicked((event) -> {
            String idioma = this.lblLenguaje.getText().substring(10, lblLenguaje.getText().length()).trim();
            if (txtUsuario.getText() == null || txtUsuario.getText().isEmpty()) {
                Mensaje.showModal(Alert.AlertType.ERROR, FlowController.getInstance().getLanguage().getString("ms.recuperarT"), (Stage) lblIniciar.getScene().getWindow(), FlowController.getInstance().getLanguage().getString("ms.recuperarM "));
            } else {
                try {
                    UsuarioService usuarioService = new UsuarioService();
                    String nuClave = Clave.getInstance().claveRandom();
                    Respuesta respuesta = usuarioService.recuperarClave(txtUsuario.getText(), nuClave);
                    if (respuesta.getEstado()) {
                        UsuarioDto us = (UsuarioDto) respuesta.getResultado("Usuario");
                        enviarCorreoRecuperacion(us.getUsCorreo(), us.getUsClave());
                    }
                } catch (Exception ex) {
                    Logger.getLogger(LogIngController.class.getName()).log(Level.SEVERE, "Error cambio clave.", ex);
                }
            }
            event.consume();
        });
    }

    public void enviarCorreoRecuperacion(String correo, String clave) {
        String idioma = this.lblLenguaje.getText().substring(10, lblLenguaje.getText().length()).trim();
        if (EnvioCorreo.getInstance().enviarCambioClave(correo, clave).getEstado()) {
            Mensaje.showConfirmation(this.idioma.getString("msCorreoT"), getStage(), this.idioma.getString("msCorreoM"));
        } else {
            Mensaje.show(Alert.AlertType.ERROR, this.idioma.getString("msCorreT2"), this.idioma.getString("msCorreM2"));
        }
    }

}
