/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.controller;

import cineuna.util.AppContext;
import cineuna.util.FlowController;
import cineuna.util.Mensaje;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Eduardo Salas Cerdas
 */
public class MenuClienteController extends Controller implements Initializable {

    @FXML
    private JFXButton btnCartelera;
    @FXML
    private JFXButton btnLogOut;
    @FXML
    private JFXButton btnSalir;
    @FXML
    private JFXButton btnMantenimiento;

    private String idioma;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        iniciliza();
        idioma = (String) AppContext.getInstance().get("idioma");
    }

    @Override
    public void initialize() {

    }

    public void iniciliza() {
        this.btnSalir.setText("Salir");
        this.btnLogOut.setText("Cerrar Sesión");
        this.btnCartelera.setText("Cartelera");
    }

    @FXML
    private void cartelera(ActionEvent event) {
        if ((WebView) AppContext.getInstance().get("web") != null) {
            ((WebView) AppContext.getInstance().get("web")).getEngine().load(null);
        }
        FlowController.getInstance().delete("Pelicula");
        FlowController.getInstance().goView("Carteleta2");
    }

    @FXML
    private void salir(ActionEvent event) {
        String titulo = "";

        String mensaje = "";
        if (idioma.equals("Es")) {
            titulo = "Salir";
            mensaje = "¿Desea  salir?";
        } else {
            titulo = "Go out";
            mensaje = "Do you want to go out?";
        }
        if (Mensaje.showConfirmation(titulo, getStage(), mensaje)) {
            ((Stage) btnSalir.getScene().getWindow()).close();
        }
    }

    @FXML
    private void LogOut(ActionEvent event) {
        String titulo = "";

        String mensaje = "";
        if (idioma.equals("Es")) {
            titulo = "Salir";
            mensaje = "¿desea cerrar sesion salir?";
        } else {
            titulo = "Go out";
            mensaje = "Do you want to log out?";
        }
        if (Mensaje.showConfirmation(titulo, getStage(), mensaje)) {
            AppContext.getInstance().set("nuevo", "NO");
            FlowController.getInstance().goLogIng();
            ((Stage) btnSalir.getScene().getWindow()).close();
        }
    }

    @FXML
    private void mantenimientoUsuarios(ActionEvent event) {
        FlowController.getInstance().goView("MantUsuario");
    }

}
