/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.service;

import cineuna.model.CineDto;
import cineuna.model.PeliculaDto;
import cineuna.util.Request;
import cineuna.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Matami
 */
public class CineService {

    public Respuesta guardarCine(CineDto cine) {
        try {
            Request re = new Request("CineController/guardarCine");
            re.post(cine);
            if (re.isError()) {
                return new Respuesta(false, re.getError(), " ");
            }
            CineDto cineDto = (CineDto) re.readEntity(CineDto.class);
            return new Respuesta(true, "", "", "Cine", cineDto);

        } catch (Exception e) {
            Logger.getLogger(CineService.class.getName()).log(Level.SEVERE, "Error registro", e);
            return new Respuesta(false, "Error registro", "guardarCine" + e.getMessage());
        }
    }

    public Respuesta getCines() {
        try {
            Map<String, Object> parametros = new HashMap<>();
            Request request = new Request("CineController/getCines", "",parametros);
            request.get();//consultar
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
             List<CineDto> cines = (List<CineDto>) request.readEntity(new GenericType<List<CineDto>>() {
            });
            return new Respuesta(true, "", "", "Cines", cines);
        } catch (Exception ex) {
            Logger.getLogger(CineService.class.getName()).log(Level.SEVERE, "Error al obtener los datos del cine : ", ex);
            return new Respuesta(false, "Error al cargar los datos del cine. ", "getCine " + ex.getMessage());
        }
    }
}
