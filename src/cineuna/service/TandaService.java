/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.service;

import cineuna.model.ReporteDto;
import cineuna.model.TandaDto;
import cineuna.util.Request;
import cineuna.util.Respuesta;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Matami
 */
public class TandaService {

    public Respuesta guardarTanda(TandaDto tandaDto) {
        try {
            Request re = new Request("TandaController/guardarTanda");
            re.post(tandaDto);
            if (re.isError()) {
                return new Respuesta(false, re.getError(), " ");
            }
            TandaDto tandaDto1 = (TandaDto) re.readEntity(TandaDto.class);
            return new Respuesta(true, "", "", "Tanda", tandaDto1);

        } catch (Exception e) {
            Logger.getLogger(TandaService.class.getName()).log(Level.SEVERE, "Error registro", e);
            return new Respuesta(false, "Error registro", "guardarTanda" + e.getMessage());
        }
    }
/**
 * Retorna la lista de las tandas asociadas a una pelicula especifica
  * @param id de la pelicula de la cual se desea conocer las tandas disponobles
 * @return 
 */
    public Respuesta getTandas(Long idpeli) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", idpeli);
            Request request = new Request("TandaController/getTandas", "/{id}", parametros);
            request.get();
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<TandaDto> tandas = (List<TandaDto>) request.readEntity(new GenericType<List<TandaDto>>() { 
            });
            return new Respuesta(true, "", "", "Tandas", tandas);
        } catch (Exception ex) {
            Logger.getLogger(TandaService.class.getName()).log(Level.SEVERE, "Error obteniendo las tandas de la pelicula especificada.", ex);
            return new Respuesta(false, "Error obteniendo las tandas de la pelicula especificada.", "getTanda " + ex.getMessage());
        }
    }
     public Respuesta getTandasFiltroFecha(LocalDateTime fecandHora) {
        try {
           String fecyHora  = fecandHora.toString();
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("fecandHora", fecyHora);
            Request request = new Request("TandaController/getTandasFiltroFecha", "/{fecandHora}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<TandaDto> tandas = (List<TandaDto>) request.readEntity(new GenericType<List<TandaDto>>() {
                
            });
            return new Respuesta(true, "", "", "Tandas", tandas);
        } catch (Exception ex) {
            Logger.getLogger(TandaService.class.getName()).log(Level.SEVERE, "Error obteniendo las tandas.", ex);
            return new Respuesta(false, "Error obteniendo las tandas.", "getTandas " + ex.getMessage());
        }
    }
     public Respuesta getReporteTanda(ReporteDto rep) {
        try {

            Request request = new Request("TandaController/reporteTanda");
            request.post(rep);

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
             ReporteDto reporte = (ReporteDto) request.readEntity(ReporteDto.class);
            return new Respuesta(true, "", "", "Reporte", reporte);
        } catch (Exception ex) {
            Logger.getLogger(TandaService.class.getName()).log(Level.SEVERE, "Error obteniendo el reporte de las peliculas.", ex);
            return new Respuesta(false, "Error obteniendo el reporte de las peliculas.", "getReporte " + ex.getMessage());
        }

    }
}
