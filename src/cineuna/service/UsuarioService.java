/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.service;

import cineuna.model.UsuarioDto;
import cineuna.util.Request;
import cineuna.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Matami
 */
public class UsuarioService {

    public UsuarioService() {
    }

    /**
     * Valida que el usuario exista
     *
     * @param usuario
     * @param clave
     * @return
     */
    public Respuesta getUsuario(String usuario, String clave) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("usuario", usuario);
            parametros.put("clave", clave);
            Request request = new Request("UsuarioController/getUsuario", "/{usuario}/{clave}", parametros);
            request.get();//consultar
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            UsuarioDto usuarioDto = (UsuarioDto) request.readEntity(UsuarioDto.class);
            return new Respuesta(true, "", "", "Usuario", usuarioDto);
        } catch (Exception ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error al obtener el usario :" + usuario + " ", ex);
            return new Respuesta(false, "Error al cargar el usuario. ", "getUusario " + ex.getMessage());
        }
    }
    public Respuesta validarUsuario(String usuario) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("usuario", usuario);
            Request request = new Request("UsuarioController/validarUsuario", "/{usuario}", parametros);
            request.get();//consultar
            if (request.isError()) {
                return new Respuesta(false,"", "");
            }
            UsuarioDto usuarioDto = (UsuarioDto) request.readEntity(UsuarioDto.class);
            if(usuarioDto==null){
               return new Respuesta(true, "", "", "","");
            }
            return new Respuesta(true, "", "", "","");
        } catch (Exception ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error al obtener el usario :" + usuario + " ", ex);
            return new Respuesta(false, "Error al cargar el usuario. ", "getUusario " + ex.getMessage());
        }
    }

    /**
     * Recupera la clave
     *
     * @param usuario
     * @return usuario
     */
    public Respuesta recuperarClave(String usuario, String nuClave) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("usuario", usuario);
            parametros.put("nuClave", nuClave);
            Request request = new Request("UsuarioController/recuperarClave", "/{usuario}/{nuClave}", parametros);
            request.get();
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            UsuarioDto usuarioDto = (UsuarioDto) request.readEntity(UsuarioDto.class);
            return new Respuesta(true, "", "", "Usuario", usuarioDto);
        } catch (Exception ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error al obtener el usuario :" + usuario + " ", ex);
            return new Respuesta(false, "Error al cargar el usuario. ", "getUusario " + ex.getMessage());
        }
    }

    /**
     * Guarda el usuario
     *
     * @param usuario
     * @return el usuario guardado
     */
    public Respuesta guardar(UsuarioDto usuario) {
        try {

            Request re = new Request("UsuarioController/guardar");
            re.post(usuario);
            if (re.isError()) {
                return new Respuesta(false, re.getError(), " ");
            }
            UsuarioDto usuarioDto = (UsuarioDto) re.readEntity(UsuarioDto.class);
            return new Respuesta(true, "", "", "Usuario", usuarioDto);

        } catch (Exception e) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error registro 1", e);
            return new Respuesta(false, "Error registro", "guardarUsuario" + e.getMessage());
        }
    }

    /**
     * Elimia el usuario
     *
     * @param id del usuario a elimiar
     * @return vacio o nulo
     */
    public Respuesta elimiarUsuario(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("UsuarioController/elimiarUsuario", "/{id}", parametros);
            request.delete();
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error al elimiar el usuario id: " + id + " ", ex);
            return new Respuesta(false, "Error al eliminar el usuario. ", "eliminarUusario " + ex.getMessage());
        }
    }

    /**
     * Modificca el atributo de administrador con recibir el id del mismo
     *
     * @param id del objeto a modificar el atributo de administrador
     * @return el elemento modificado
     */
    public Respuesta modificarAdministrador(Long id) {
        try {
            Map<String, Object> par = new HashMap<>();
            par.put("id", id);
            Request re = new Request("UsuarioController/modificarAdministrador", "/{id}", par);
            re.get();
            if (re.isError()) {
                return new Respuesta(false, re.getError(), "");
            }
            UsuarioDto usuarioDto = (UsuarioDto) re.readEntity(UsuarioDto.class);
            return new Respuesta(true, "", "", "Usuario", usuarioDto);
        } catch (Exception e) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error al modificar el usuario", e);
            return new Respuesta(false, "Error al modificar el usuario", "modificarUsuario" + e.getMessage());
        }
    }
    /**
     * busca todos los usuarios que estan activos y los retorna
     * @return lista de usuarios activos 
     */
    public Respuesta getUsuariosActivos() {
        try {
            Map<String, Object> parametros = new HashMap<>();
            Request request = new Request("UsuarioController/getUsuariosActivos", "", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<UsuarioDto> usuarios = (List<UsuarioDto>) request.readEntity(new GenericType<List<UsuarioDto>>() {
            });
            return new Respuesta(true, "", "", "Usuarios", usuarios);
        } catch (Exception ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error obteniendo los usuarios.", ex);
            return new Respuesta(false, "Error obteniendo los usuarios.", "getUsuarios " + ex.getMessage());
        }
    }
     
}
