/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.service;

import cineuna.model.BoletoDto;
import cineuna.model.CompraDto;
import cineuna.util.Request;
import cineuna.util.Respuesta;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Matami
 */
public class CompraService {

    public CompraService() {
    }

    /**
     * busca el boleto por el id de la tanda y la fecha que se desea filtrar.
     *
     * @param nAsiento
     * @return Retorna una lista actualizada de todos los boletos
     *
     */
    public Respuesta getBoleto(Long idTanda, LocalDate fech) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("idTanda", idTanda);
            String fecha = fech.toString();
            parametros.put("fecha", fecha);
            Request request = new Request("CompraController/getCompras", "{idTanda}/{fecha}", parametros);
            request.get();
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            List<BoletoDto> boletos = (List<BoletoDto>) request.readEntity(new GenericType<List<BoletoDto>>() {
            });
            return new Respuesta(true, "", "", "Boletos", boletos);
        } catch (Exception ex) {
            Logger.getLogger(CompraService.class.getName()).log(Level.SEVERE, "Error al obtener el boleto :" + idTanda + " ", ex);
            return new Respuesta(false, "Error al cargar el boleto. ", "getBoleto " + ex.getMessage());
        }
    }

    /**
     * recibe el boleto a actualizar
     *
     * @param
     * @return retorna una lista para actualiar el estado de todos los
     */
    public Respuesta guardarCompra(BoletoDto boleto) {
        try {
            Request re = new Request("CompraController/guardarCompra");
            re.post(boleto);
            if (re.isError()) {
                return new Respuesta(false, re.getError(), "");
            }
            CompraDto compra = (CompraDto) re.readEntity(CompraDto.class);

            return new Respuesta(true, "", "", "Compra", compra);
        } catch (Exception e) {
            Logger.getLogger(CompraService.class.getName()).log(Level.SEVERE, "Error registro 1", e);
            return new Respuesta(false, "Error registro", "guardarBoleto" + e.getMessage());
        }
    }

    /**
     * Elimina una compra
     *
     * @param idCom de la compra a eliminar
     * @return
     */
    public Respuesta elimiarCompra(Long idCom) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("idCom", idCom);
            Request request = new Request("CompraController/elimiarCompra", "/{idCom}", parametros);
            request.delete();
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(CompraService.class.getName()).log(Level.SEVERE, "Error al elimiar el usuario id: " + idCom + " ", ex);
            return new Respuesta(false, "Error al eliminar el usuario. ", "eliminarUusario " + ex.getMessage());
        }
    }
/**
 * Envia desde el web service un correo con el comprobante
 * @param compra Compra o factura
 * @param mensaje el mensaje del correo
 * @param nombreArch nombre del archivo
 * @return 
 */
    public Respuesta confirmarCompra(CompraDto compra, String mensaje, String nombreArch) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("mensaje", mensaje);
            parametros.put("nombreArch", nombreArch);
            Request request = new Request("CompraController/confirmarCompra", "/{mensaje}/{nombreArch}", parametros);
            request.post(compra);
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(CompraService.class.getName()).log(Level.SEVERE, "Error al elimiar el usuario id: " , ex);
            return new Respuesta(false, "Error al eliminar el usuario. ", "eliminarUusario " + ex.getMessage());
        }
    }
}
