/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.service;

import cineuna.model.PeliculaDto;
import cineuna.model.ReporteDto;
import cineuna.util.Request;
import cineuna.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Matami
 */
public class PeliculaService {

    public PeliculaService() {
    }

    /**
     * guarda de las peliculas
     *
     * @param pelicula
     * @return
     */
    public Respuesta guardar(PeliculaDto pelicula) {
        try {
            Request re = new Request("PeliculaController/guardarPelicula");
            re.post(pelicula);
            if (re.isError()) {
                return new Respuesta(false, re.getError(), "");
            }
            PeliculaDto pelicula1 = (PeliculaDto) re.readEntity(PeliculaDto.class);
            return new Respuesta(true, "Guardado correcto", "", "Pelicula", pelicula1);
        } catch (Exception e) {
            Logger.getLogger(PeliculaService.class.getName()).log(Level.SEVERE, "Error registro 1", e);
            System.out.println(e.getMessage());
            return new Respuesta(false, "Error registro" + e.getMessage(), "guardarPelicula" + e.getMessage());
        }
    }

    /**
     * retorna todas las peliculas que existen
     *
     * @return
     */
    public Respuesta getPeliculasList() {
        try {
            Map<String, Object> parametros = new HashMap<>();
            Request request = new Request("PeliculaController/getPeliculasList", "", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<PeliculaDto> peliculas = (List<PeliculaDto>) request.readEntity(new GenericType<List<PeliculaDto>>() {
            });
            return new Respuesta(true, "", "", "Peliculas", peliculas);
        } catch (Exception ex) {
            Logger.getLogger(PeliculaService.class.getName()).log(Level.SEVERE, "Error obteniendo las peliculas.", ex);
            return new Respuesta(false, "Error obteniendo las peliculas.", "getPeliiculas " + ex.getMessage());
        }
    }
    
    /**
     * retorna todas las peliculas que existen con un mismo genero o aproximado
     *
     * @return
     */
    public Respuesta getPeliculasFiltroG(String genero) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("genero", genero);
            Request request = new Request("PeliculaController/getPeliculasFiltroG", "/{genero}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<PeliculaDto> peliculas = (List<PeliculaDto>) request.readEntity(new GenericType<List<PeliculaDto>>() {

            });
            return new Respuesta(true, "", "", "Peliculas", peliculas);
        } catch (Exception ex) {
            Logger.getLogger(PeliculaService.class.getName()).log(Level.SEVERE, "Error obteniendo las peliculas.", ex);
            return new Respuesta(false, "Error obteniendo las peliculas.", "getPeliiculas " + ex.getMessage());
        }
    }
  
/**
 * Generador de reporte de peliculas
 * @return 
 */
    public Respuesta getReportePeli(Long idPeli) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("idPeli", idPeli);
            Request request = new Request("PeliculaController/reporte", "/{idPeli}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
             ReporteDto reporte = (ReporteDto) request.readEntity(ReporteDto.class);
            return new Respuesta(true, "", "", "Reporte", reporte);
        } catch (Exception ex) {
            Logger.getLogger(PeliculaService.class.getName()).log(Level.SEVERE, "Error obteniendo el reporte de las peliculas.", ex);
            return new Respuesta(false, "Error obteniendo el reporte de las peliculas.", "getReporte " + ex.getMessage());
        }

    }
}
