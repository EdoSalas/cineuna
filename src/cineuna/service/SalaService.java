/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.service;

import cineuna.model.AsientoDto;
import cineuna.model.SalaDto;
import cineuna.util.Request;
import cineuna.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Matami
 */
public class SalaService {

    public SalaService() {
    }

    /**
     * Guardala sala
     *
     * @param salaDto
     * @return
     */
    public Respuesta guardar(SalaDto sala) {
        try {

            Request re = new Request("SalaController/guardar");
            re.post(sala);
            if (re.isError()) {
                return new Respuesta(false, re.getError(), "");
            }
            SalaDto salaDto = (SalaDto) re.readEntity(SalaDto.class);
            return new Respuesta(true, "", "", "Sala", salaDto);
        } catch (Exception e) {
            Logger.getLogger(SalaService.class.getName()).log(Level.SEVERE, "Error registro 1", e);
            return new Respuesta(false, "Error registro", "guardarSala" + e.getMessage());
        }
    }

    /**
     * Carga la sala indicada con el id
     *
     * @return sala unica
     * @param id tipo long
     */
    public Respuesta getSala(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("SalaController/getSala", "/{id}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            SalaDto salaDto = (SalaDto) request.readEntity(SalaDto.class);
//            
            return new Respuesta(true, "", "", "Sala", salaDto);
        } catch (Exception ex) {
            Logger.getLogger(SalaService.class.getName()).log(Level.SEVERE, "Error obteniendo las salas", ex);
            return new Respuesta(false, "Error obteniendo las salas.", "getSalas " + ex.getMessage());
        }
    }

    /**
     * Carga todas las salas
     *
     * @return Lista de salas
     */
    public Respuesta getSalas() {
        try {
            Map<String, Object> parametros = new HashMap<>();
            Request request = new Request("SalaController/getSalas", "", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<SalaDto> salas = (List<SalaDto>) request.readEntity(new GenericType<List<SalaDto>>() {
            });
            return new Respuesta(true, "", "", "Salas", salas);
        } catch (Exception ex) {
            Logger.getLogger(SalaService.class.getName()).log(Level.SEVERE, "Error obteniendo las salas.", ex);
            return new Respuesta(false, "Error obteniendo las salas.", "getSalas " + ex.getMessage());
        }
    }

    public Respuesta eliminarSalas(List<SalaDto> salas) {
        try {
            Request re = new Request("SalaController/eliminarSalas");
//            re.post(salas);
            re.delete();//consultar
            if (re.isError()) {
                return new Respuesta(false, re.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error al elimiar el usuario id: ", ex);
            return new Respuesta(false, "Error al eliminar el usuario. ", "eliminarUusario " + ex.getMessage());
        }
    }
}
