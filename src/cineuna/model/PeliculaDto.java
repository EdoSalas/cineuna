/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.model;

import cineuna.util.LocalDateAdapter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Matami
 */
@XmlRootElement(name = "PeliculaDto")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class PeliculaDto {

    @XmlTransient
    public SimpleStringProperty peId;
    @XmlTransient
    public SimpleStringProperty peNombreEs;
    @XmlTransient
    public SimpleStringProperty peNombreIn;
    @XmlTransient
    public ObjectProperty<String> peIdioma;
    @XmlTransient
    public SimpleStringProperty peResennaEs;
    @XmlTransient
    public SimpleStringProperty peResennaIn;
    @XmlTransient
    public SimpleStringProperty peGenero;
    @XmlTransient
    public SimpleStringProperty peLinkEs;
    @XmlTransient
    public SimpleStringProperty peLinkIn;
    @XmlTransient
    public ObjectProperty<LocalDate> peFestreno;
//    @XmlTransient
//    private Image peImage;
    @XmlTransient
    private String peImagenS;
    @XmlTransient
    public ObjectProperty<String> peEstado;
    @XmlTransient
    private List<TandaDto> tandaList = new ArrayList();

    public PeliculaDto() {
        this.peId = new SimpleStringProperty();
        this.peNombreEs = new SimpleStringProperty();
        this.peNombreIn = new SimpleStringProperty();
        this.peIdioma = new SimpleObjectProperty("Es");
        this.peResennaEs = new SimpleStringProperty();
        this.peResennaIn = new SimpleStringProperty();
        this.peLinkEs = new SimpleStringProperty();
        this.peLinkIn = new SimpleStringProperty();
        this.peEstado = new SimpleObjectProperty("D");
        this.peFestreno = new SimpleObjectProperty();
        this.peGenero = new SimpleStringProperty();
    }

    public PeliculaDto(String  peImage, String nombreEs, String nombreEn, String descripEs, String descripEn, String urlEs, String urlEn, String estado, String genero, LocalDate estreno) {
        this.peId = new SimpleStringProperty();
//        this.peImage = peImage;
        peImagenS = peImage;
        this.peNombreEs = new SimpleStringProperty(nombreEs);
        this.peNombreIn = new SimpleStringProperty(nombreEn);
        this.peResennaEs = new SimpleStringProperty(descripEs);
        this.peResennaIn = new SimpleStringProperty(descripEn);
        this.peLinkEs = new SimpleStringProperty(urlEs);
        this.peLinkIn = new SimpleStringProperty(urlEn);
        this.peEstado = new SimpleObjectProperty(estado);
        this.peGenero = new SimpleStringProperty(genero);
        this.peFestreno = new SimpleObjectProperty(estreno);
        this.peIdioma = new SimpleObjectProperty("Multi");
    }

    public Long getPeId() {

        if (peId.get() != null && !peId.get().isEmpty()) {
            return Long.valueOf(peId.get());
        } else {
            return null;
        }
    }

    public void setPeId(Long peId) {
        this.peId.set(peId.toString());
    }

    public String getPeNombreEs() {
        return peNombreEs.get();
    }

    public void setPeNombreEs(String peNombreEs) {
        this.peNombreEs.set(peNombreEs);
    }

    public String getPeNombreIn() {
        return peNombreIn.get();
    }

    public void setPeNombreIn(String peNombreIn) {
        this.peNombreIn.set(peNombreIn);
    }

    public String getPeIdioma() {
        return peIdioma.get();
    }

    public void setPeIdioma(String peIdioma) {
        this.peIdioma.set(peIdioma);
    }

    public String getPeResennaEs() {
        return peResennaEs.get();
    }

    public void setPeResennaEs(String peResennaEs) {
        this.peResennaEs.set(peResennaEs);
    }

    public String getPeResennaIn() {
        return peResennaIn.get();
    }

    public void setPeResennaIn(String peResennaIn) {
        this.peResennaIn.set(peResennaIn);
    }

    public String getPeLinkEs() {
        return peLinkEs.get();
    }

    public void setPeLinkEs(String peLinkEs) {
        this.peLinkEs.set(peLinkEs);
    }

    public String getPeLinkIn() {
        return peLinkIn.get();
    }

    public void setPeLinkIn(String peLinkIn) {
        this.peLinkIn.set(peLinkIn);
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDate getPeFestreno() {
        return peFestreno.get();
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public void setPeFestreno(LocalDate peFestreno) {
        this.peFestreno.set(peFestreno);
    }

//    public Image getPeImage() {
//        return peImage;
//    }
//
//    public void setPeImage(Image peImage) {
//        this.peImage = peImage;
//    }

    public String  getPeImagenS() {
        return peImagenS;
    }

    public void setPeImagenS(String  peImagenS) {
        this.peImagenS = peImagenS;
    }

    public String getPeEstado() {
        return peEstado.get();
    }

    public void setPeEstado(String peEstado) {
        this.peEstado.set(peEstado);
    }

    public String getPeGenero() {
        return peGenero.get();
    }

    public void setPeGenero(String peGenero) {
        this.peGenero.set(peGenero);
    }

    public List<TandaDto> getTandaList() {
        return tandaList;
    }

    public void setTandaList(List<TandaDto> tandaList) {
        this.tandaList = tandaList;
    }

    @Override
    public String toString() {
        return peNombreEs.get();
    }

 

}
