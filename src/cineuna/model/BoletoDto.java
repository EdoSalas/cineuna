/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Matami
 */
@XmlRootElement(name = "BoletoDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class BoletoDto {

    private Long boId;
    private String boFilaAsiento;
    private Integer boNumeroAsiento;
    private String boEstado;
    private Boolean modificado = false;

    public BoletoDto() {
    }

    public Long getBoId() {
        return boId;
    }

    public void setBoId(Long boId) {
        this.boId = boId;
    }

    public String getBoEstado() {
        return boEstado;
    }

    public void setBoEstado(String boEstado) {
        this.boEstado = boEstado;
    }

    public String getBoFilaAsiento() {
        return boFilaAsiento;
    }

    public void setBoFilaAsiento(String boFilaAsiento) {
        this.boFilaAsiento = boFilaAsiento;
    }

    public Integer getBoNumeroAsiento() {
        return boNumeroAsiento;
    }

    public void setBoNumeroAsiento(Integer boNumeroAsiento) {
        this.boNumeroAsiento = boNumeroAsiento;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

}
