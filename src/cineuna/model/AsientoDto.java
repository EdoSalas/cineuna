/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.model;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Matami
 */
@XmlRootElement(name = "AsientoDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class AsientoDto{

    private Long asId;
    private String asFila;
    private Integer asNumAsiento;
    private String asEstado;
     private List<BoletoDto> boletos;

    public AsientoDto() {
        this.asEstado = "D";
    }

    public Long getAsId() {
        return asId;
    }

    public void setAsId(Long asId) {
        this.asId = asId;
    }

    public String getAsFila() {
        return asFila;
    }

    public void setAsFila(String asFila) {
        this.asFila = asFila;
    }

    public String getAsEstado() {
        return asEstado;
    }

    public void setAsEstado(String asEstado) {
        this.asEstado = asEstado;
    }

    public Integer getAsNumAsiento() {
        return asNumAsiento;
    }

    public void setAsNumAsiento(Integer asNumAsiento) {
        this.asNumAsiento = asNumAsiento;
    }
    public List<BoletoDto> getBoletos() {
        return boletos;
    }

    public void setBoletos(List<BoletoDto> boletos) {
        this.boletos = boletos;
    }

}
