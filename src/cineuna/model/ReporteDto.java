/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.model;

import cineuna.util.LocalDateAdapter;
import cineuna.util.LocalDateTimeAdapter;
import java.time.LocalDate;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Matami
 */
@XmlRootElement(name = "ReporteDto")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class ReporteDto {

    @XmlTransient
    public ObjectProperty<LocalDate> fecha1;
    @XmlTransient
    public ObjectProperty<LocalDate> fecha2;
    @XmlTransient
    private String arCode;

    public ReporteDto() {
        arCode = "";
        this.fecha1 = new SimpleObjectProperty();
        this.fecha2 = new SimpleObjectProperty();
    }

    public ReporteDto(String arCode) {
        this.arCode = arCode;
        this.fecha1 = new SimpleObjectProperty();
        this.fecha2 = new SimpleObjectProperty();
    }

    public String getArCode() {
        return arCode;
    }

    public void setArCode(String arCode) {
        this.arCode = arCode;
    }

  
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDate getFecha1() {
        return fecha1.get();
    }
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public void setFecha1(LocalDate fecha1) {
        this.fecha1.set(fecha1);
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDate getFecha2() {
        return fecha2.get();
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public void setFecha2(LocalDate fecha2) {
        this.fecha2.set(fecha2);
    }

}
