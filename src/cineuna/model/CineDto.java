/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.model;

import cineuna.util.LocalDateTimeAdapter;
import java.time.LocalDateTime;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Matami
 */
@XmlRootElement(name = "CineDto")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class CineDto {
    @XmlTransient
    public SimpleStringProperty cid;
    @XmlTransient
    public SimpleStringProperty cNombre;
    @XmlTransient
    public ObjectProperty<LocalDateTime> cHorai;
    @XmlTransient
    public ObjectProperty<LocalDateTime> cHoraf;
    @XmlTransient
    public SimpleStringProperty correo;
    @XmlTransient
    public SimpleStringProperty cTelefono;

    public CineDto() {
        this.cid = new SimpleStringProperty();
        this.cNombre = new SimpleStringProperty();
        this.cHorai =  new SimpleObjectProperty();
        this.cHoraf =  new SimpleObjectProperty();
        this.correo = new SimpleStringProperty();
        this.cTelefono = new SimpleStringProperty();
    }

    public Long getCid() {
       if(this.cid.get()!=null&&!cid.get().isEmpty()){
           return Long.valueOf(cid.get());
       }
       else{
           return null;
       }
    }

    public void setCid(Long cid) {
        this.cid.set(cid.toString());
    }

    public String getcNombre() {
        return cNombre.get();
    }

    public void setcNombre(String cNombre) {
        this.cNombre.set(cNombre);
    }
//    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public String getcHorai() {
        return cHorai.get().toString();
    }

//    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public void setcHorai(String cHorai) {
        this.cHorai.set(LocalDateTime.parse(cHorai));
    }
//   @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public String getcHoraf() {
        return cHoraf.get().toString();
    }
//   @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public void setcHoraf(String cHoraf) {
        this.cHoraf.set(LocalDateTime.parse(cHoraf));
    }


    public String getCorreo() {
        return correo.get();
    }

    public void setCorreo(String correo) {
        this.correo.set(correo);
    }

    public Integer getcTelefono() {
        if(cTelefono.get()!=null&&!cTelefono.get().isEmpty()){
            return Integer.valueOf(cTelefono.get());
        }else{
            return null;
        }
    }

    public void setcTelefono(Integer cTelefono) {
        this.cTelefono.set(cTelefono.toString());
    }

    @Override
    public String toString() {
        return "CineDto{" + "cid=" + cid + ", cNombre=" + cNombre + ", cHorai=" + cHorai + ", cHoraf=" + cHoraf + ", correo=" + correo + ", cTelefono=" + cTelefono + '}';
    }
    
}
