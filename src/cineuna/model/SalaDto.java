/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.model;

import com.jfoenix.controls.JFXButton;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Matami
 */
@XmlRootElement(name = "SalaDto")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class SalaDto{

    @XmlTransient
    public SimpleStringProperty saId;
    @XmlTransient
    public SimpleStringProperty saNombre;
    @XmlTransient
    public SimpleStringProperty saFonImagen;
    @XmlTransient
    public ObjectProperty<String> saEstado;
    @XmlTransient
    private List<TandaDto> tandas = new ArrayList();
    //@XmlTransient
    public List<AsientoDto> asientos = new ArrayList();
    @XmlTransient
    private List<AsientoDto> elimados = new ArrayList();

    public SalaDto() {
        this.saId = new SimpleStringProperty();
        this.saNombre = new SimpleStringProperty();
        this.saEstado = new SimpleObjectProperty();
        this.saFonImagen =  new SimpleStringProperty();
    }
    public Long getSaId() {
        if (saId.get() != null && !saId.get().isEmpty()) {
            return Long.valueOf(saId.get());
        } else {
            return null;
        }
    }

    public void setSaId(Long saId) {
        this.saId.set(saId.toString());
    }

    public String getSaNombre() {
        return saNombre.get();
    }

    public void setSaNombre(String saNombre) {
        this.saNombre.set(saNombre);
    }

    public String getSaEstado() {
        return saEstado.get();
    }

    public void setSaEstado(String saEstado) {
        this.saEstado.set(saEstado);
    }

    public List<TandaDto> getTandas() {
        return tandas;
    }

    public void setTandas(List<TandaDto> tandas) {
        this.tandas = tandas;
    }
@XmlTransient
    public List<AsientoDto> getAsientos() {
        return asientos;
    }
@XmlTransient
    public void setAsientos(List<AsientoDto> asientos) {
        this.asientos = asientos;
    }

    public List<AsientoDto> getElimados() {
        return elimados;
    }

    public void setElimados(List<AsientoDto> elimados) {
        this.elimados = elimados;
    }

    public List<AsientoDto> getElinados() {
        return elimados;
    }

    public void setElinados(List<AsientoDto> elinados) {
        this.elimados = elinados;
    }

    public String getSaFonImagen() {
        return saFonImagen.get();
    }

    public void setSaFonImagen(String saFonImagen) {
        this.saFonImagen.set(saFonImagen);
    }


}
