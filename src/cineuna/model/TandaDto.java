/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.model;

import java.time.LocalDateTime;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
/**
 *
 * @author Matami
 */
@XmlRootElement(name = "TandaDto")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class TandaDto {
    @XmlTransient
     public SimpleStringProperty taId;
    @XmlTransient
    public SimpleStringProperty  taIdioma;
    @XmlTransient
     public SimpleStringProperty  taCosto;
    @XmlTransient
    public ObjectProperty<LocalDateTime> taFhinicio;
    @XmlTransient
   public ObjectProperty<LocalDateTime> taFhfin;
    @XmlTransient
    private List<BoletoDto> boletoList;
    @XmlTransient
    private PeliculaDto taIdPefk;
    @XmlTransient
    private SalaDto taIdSafk;

    public TandaDto() {
        this.taId = new SimpleStringProperty();
        this.taIdioma = new SimpleStringProperty();
        this.taCosto = new SimpleStringProperty();
        this.taFhinicio = new SimpleObjectProperty();
        this.taFhfin = new SimpleObjectProperty();
    }

    
    public Long getTaId() {
        if(taId.get()!=null&&!taId.get().isEmpty()){
            return Long.valueOf(taId.get());
        }
        else{
            return null;
        }
    }

    public void setTaId(Long taId) {
        this.taId.set(taId.toString());
    }

    public String getTaIdioma() {
        return taIdioma.get();
    }

    public void setTaIdioma(String taIdioma) {
        this.taIdioma.set(taIdioma);
    }

    public Integer getTaCosto() {
        if(taCosto.get()!=null&&!taCosto.get().isEmpty()){
            return Integer.valueOf(taCosto.get());
        }
        else{
            return null;
        }
    }

    public void setTaCosto(Integer taCosto) {
        this.taCosto.set(taCosto.toString());
    }
//    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public String getTaFhinicio() {
        return taFhinicio.get().toString();
    }
//    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public void setTaFhinicio(String taFhinicio) {
        this.taFhinicio.set(LocalDateTime.parse(taFhinicio));
    }
//    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public String getTaFhfin() {
        return taFhfin.get().toString();
    }

//    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public void setTaFhfin(String taFhfin) {
        this.taFhfin.set(LocalDateTime.parse(taFhfin));
    }

    public List<BoletoDto> getBoletoList() {
        return boletoList;
    }

    public void setBoletoList(List<BoletoDto> boletoList) {
        this.boletoList = boletoList;
    }

    public PeliculaDto getTaIdPefk() {
        return taIdPefk;
    }

    public void setTaIdPefk(PeliculaDto taIdPefk) {
        this.taIdPefk = taIdPefk;
    }


    public SalaDto getTaIdSafk() {
        return taIdSafk;
    }

    public void setTaIdSafk(SalaDto taIdSafk) {
        this.taIdSafk = taIdSafk;
    }

  

    @Override
    public String toString() {
        return "TandaDto{" + "taId=" + taId + ", taIdioma=" + taIdioma + ", taCosto=" + taCosto + ", taFhinicio=" + taFhinicio + ", taFhfin=" + taFhfin + ", boletoList=" + boletoList + ", taIdPefk=" + taIdPefk + ", taIdSafk=" + taIdSafk + '}';
    }
    
    
}
