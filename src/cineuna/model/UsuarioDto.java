/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.model;

import java.math.BigInteger;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Matami
 */
@XmlRootElement(name = "UsuarioDto")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class UsuarioDto {
    @XmlTransient
    public SimpleStringProperty usId;
    @XmlTransient
    public SimpleStringProperty usNombre;
    @XmlTransient
    public SimpleStringProperty usApellido;
    @XmlTransient
    public SimpleStringProperty usCedula;
    @XmlTransient
    public SimpleStringProperty usTelefono;
    @XmlTransient
    public SimpleStringProperty usUsuario;
    @XmlTransient
    public SimpleStringProperty usClave;
    @XmlTransient
    public SimpleStringProperty usCorreo;
    @XmlTransient
    public SimpleStringProperty usRecuperarClave;//
    @XmlTransient
    public SimpleStringProperty usCodigoActivacion;
    @XmlTransient
    public ObjectProperty<String> usIdioma;
    @XmlTransient
    public ObjectProperty<String> usActivo; 
    @XmlTransient
    public ObjectProperty<String> usGenero;
    @XmlTransient
    public ObjectProperty<String> usAdministrador;
  
    

    public UsuarioDto() {
        this.usId = new SimpleStringProperty();
        this.usNombre = new SimpleStringProperty();
        this.usApellido = new SimpleStringProperty();
        this.usCedula = new SimpleStringProperty();
        this.usTelefono = new SimpleStringProperty();
        this.usUsuario = new SimpleStringProperty();
        this.usClave = new SimpleStringProperty();
        this.usCorreo = new SimpleStringProperty();
        this.usIdioma = new SimpleObjectProperty("Es");
        this.usActivo = new SimpleObjectProperty("I");
        this.usAdministrador = new SimpleObjectProperty("N");
        this.usGenero = new SimpleObjectProperty("M");
        this.usRecuperarClave = new SimpleStringProperty("N");
        this.usCodigoActivacion = new SimpleStringProperty();
    }

    public Long getUsId() {
       if(usId.get()!=null&&!usId.get().isEmpty()){
           return Long.valueOf(usId.get());
       }
       else{
           return null;
       }
    }

    public void setUsId(Long usId) {
        this.usId.set(usId.toString());
    }

    public String getUsNombre() {
        return usNombre.get();
    }

    public void setUsNombre(String usNombre) {
        this.usNombre.set(usNombre);
    }

    public String getUsApellido() {
        return usApellido.get();
    }

    public void setUsApellido(String usApellido) {
        this.usApellido.set(usApellido);
    }

    public String getUsCedula() {
        return usCedula.get();
    }

    public void setUsCedula(String usCedula) {
        this.usCedula.set(usCedula);
    }

    public Integer getUsTelefono() {
        if(usTelefono.get()!=null&&!usTelefono.get().isEmpty()){
            return Integer.valueOf(usTelefono.get());
        }
        else{
            return null;
        }
    }

    public void setUsTelefono(Integer usTelefono) {
        this.usTelefono.set(usTelefono.toString());
    }

    public String getUsUsuario() {
        return usUsuario.get();
    }

    public void setUsUsuario(String usUsuario) {
        this.usUsuario.set(usUsuario);
    }

    public String getUsClave() {
        return usClave.get();
    }

    public void setUsClave(String usClave) {
        this.usClave.set(usClave);
    }

    public String getUsCorreo() {
        return usCorreo.get();
    }

    public void setUsCorreo(String usCorreo) {
        this.usCorreo.set(usCorreo);
    }

    public String getUsIdioma() {
        return usIdioma.get();
    }

    public void setUsIdioma(String usIdioma) {
        this.usIdioma.set(usIdioma);
    }

    public String getUsActivo() {
        return usActivo.get();
    }

    public void setUsActivo(String usActivo) {
        this.usActivo.set(usActivo);
    }
    public String getUsGenero() {
        return usGenero.get();
    }

    public void setUsGenero(String usGenero) {
        this.usGenero.set(usGenero);
    }
    public String getUsAdministrador() {
        return usAdministrador.get();
    }

    public void setUsAdministrador(String usAdministrador) {
        this.usAdministrador.set(usAdministrador);
    }

    public String getUsRecuperarClave() {
        return usRecuperarClave.get();
    }

    public void setUsRecuperarClave(String usRecuperarClave) {
        this.usRecuperarClave.set(usRecuperarClave);
    }

    public String getUsCodigoActivacion() {
        return usCodigoActivacion.get();
    }

    public void setUsCodigoActivacion(String usCodigoActivacion) {
        this.usCodigoActivacion.set(usCodigoActivacion);
    }
  

    @Override
    public String toString() {
        return "UsuarioDto{" + "usId=" + usId + ", usNombre=" + usNombre + ", usApellido=" + usApellido + ", usCedula=" + usCedula + ", usTelefono=" + usTelefono + ", usUsuario=" + usUsuario + ", usClave=" + usClave + ", usCorreo=" + usCorreo + ", usIdioma=" + usIdioma + ", usActivo=" + usActivo + ", usAdministrador=" + usAdministrador + '}';
    }

   
}
