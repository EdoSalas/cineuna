/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.util;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceDialog;
import javafx.stage.StageStyle;

/**
 *
 * @author Eduardo Salas Cerdas
 */
public class Funciones {
    //Valida solo numeros o letras si el booleano es true son numeros si es false son letras
    public static boolean isNumber(String str) {
        char c = str.charAt(0);
        return Character.isDigit(c);
    }

    public static boolean esNumero(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");
    }

    public static boolean esLetra(String str) {
        char c = str.charAt(0);
        return Character.isLetter(c);
    }

    public static boolean letraSinTilde(String str) {
        return str.equalsIgnoreCase("á") || str.equalsIgnoreCase("é") || str.equalsIgnoreCase("í") || str.equalsIgnoreCase("ó") || str.equalsIgnoreCase("ú");
    }

    //Crea un vector de tamaño n con numeros aleatorios sin repetirse
    public static int[] rand(int from, int to) {
        int n = to - from + 1;
        int a[] = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = i;
        }
        int[] result = new int[n];
        int x = n;
        SecureRandom rd = new SecureRandom();
        for (int i = 0; i < n; i++) {
            int k = rd.nextInt(x);
            result[i] = a[k];
            a[k] = a[x - 1];
            x--;
        }
        return result;
    }

    public static void mensajeError(String cuerpo) {
        Alert error = new Alert(Alert.AlertType.ERROR);
        error.setTitle("¡Error!");
        error.setHeaderText("Ha ocurrido un error");
        error.setContentText(cuerpo);
        error.initStyle(StageStyle.UTILITY);
        java.awt.Toolkit.getDefaultToolkit().beep();
        error.show();
    }

    public static void mensajeAdvertencia(String cuerpo) {
        Alert error = new Alert(Alert.AlertType.WARNING);
        error.setTitle("¡Alerta!");
        error.setHeaderText("Ha ocurrido un error");
        error.setContentText(cuerpo);
        error.initStyle(StageStyle.UTILITY);
        java.awt.Toolkit.getDefaultToolkit().beep();
        error.show();
    }

    public static void mensajeExito(String cuerpo) {
        Alert error = new Alert(Alert.AlertType.INFORMATION);
        error.setTitle("¡Información!");
        error.setHeaderText("Transacción Exitosa");
        error.setContentText(cuerpo);
        error.initStyle(StageStyle.UTILITY);
        java.awt.Toolkit.getDefaultToolkit().beep();
        error.show();
    }
    
    public static String mensajeCombo(ArrayList<String> selecciones) {
        String letra = "";
        ChoiceDialog<String> dialog = new ChoiceDialog<>("A", selecciones);
        dialog.setHeaderText("Sustitución del espacio en blanco");
        dialog.setContentText("Elije la letra a utilizar:");

        // Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            letra = result.get();
        }
        return letra;
    }
}
