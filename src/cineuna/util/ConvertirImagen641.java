/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.util;

/**
 *
 * @author Matami
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Base64;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;
import java.io.IOException;
public class ConvertirImagen641 {


    /**
     * Recibe el string de la imagen y lo pasa a archivo Image
     *
     * @param imageString String de la imagen
     * @return retorna el archivo image
     */
    public Image stringToImage(String imageString) {
        Image im;

        BufferedImage image = null;
        byte[] imageByte;
        try {
             Base64.Decoder decoder = Base64.getDecoder();
            imageByte = decoder.decode(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return im = SwingFXUtils.toFXImage(image, null);
    }

    /**
     * Pasa de el string de la imagen al bufer de la misma
     *
     * @param imageString
     * @return
     */
    public BufferedImage stringToBufferedImage(String imageString) {

        BufferedImage image = null;
        byte[] imageByte;
        try {
            Base64.Decoder decoder = Base64.getDecoder();
            imageByte = decoder.decode(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }

    /**
     * Pasa de imagen a String
     *
     * @param im recibe una Image
     * @param tipoImage Tipo de imagen
     * @return El string de la imagen
     */
    public String imageToString(Image im) {

        BufferedImage image = new BufferedImage(400, 400, BufferedImage.TYPE_INT_RGB);
        //pasa de imagen a buffer o al reves
        image = SwingFXUtils.fromFXImage(im, null);

        String imageString = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, "png", bos);
            byte[] imageBytes = bos.toByteArray();

            Base64.Encoder encode = Base64.getEncoder();
            imageString = encode.encodeToString(imageBytes);

            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageString;
    }
    public  void crearImagen( BufferedImage im) {
        
		File fichero = new File("foto.jpg");
		String formato = "jpg";
                
		// Escribimos la imagen en el archivo.
		try {
			ImageIO.write(im, "png", fichero);
		} catch (IOException e) {
			System.out.println("Error de escritura");
		}
	}
}
