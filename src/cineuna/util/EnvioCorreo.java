/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author Matami
 */
public class EnvioCorreo {

    private static EnvioCorreo INSTANCE = null;
    private String correoAqui = "spaceview2018gmail.com";
    private String contra = "80MK002CUS";

    private static void createInstance() {
        if (INSTANCE == null) {
            synchronized (EnvioCorreo.class) {
                if (INSTANCE == null) {
                    INSTANCE = new EnvioCorreo();
                }
            }
        }
    }

    public static EnvioCorreo getInstance() {
        if (INSTANCE == null) {
            createInstance();
        }
        return INSTANCE;
    }

    /**
     * envia el correo de para la activacion del usuario
     *
     * @param correo
     * @param id
     * @throws FileNotFoundException
     */
    public Respuesta enviarActivacion(String correo, String us) throws IOException {
        String di = muestraContenido();
        String dirActivacion = ("Para activar su cuenta ingrese al siguiente enlace\n\n"
                + di + "UsuarioController/activar/" + us);

        try {
            // Propiedades de la conexión
            Properties props = new Properties();
            props.setProperty("mail.smtp.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.user", "spaceview2018@gmail.com");
            props.setProperty("mail.smtp.auth", "true");

            // Preparamos la sesion
            Session session = Session.getDefaultInstance(props);

            // Construimos el mensaje
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress("spaceview2018@gmail.com"));
            message.addRecipient(
                    Message.RecipientType.TO,
                    new InternetAddress(correo));
            message.setSubject("Activacion");
            message.setText(dirActivacion);

            // Lo enviamos.
            Transport t = session.getTransport("smtp");
            t.connect("spaceview2018@gmail.com", "80MK002CUS");
            t.sendMessage(message, message.getAllRecipients());
            // Cierre.
            t.close();
            return new Respuesta(true, "", "");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new Respuesta(false, e.getMessage(), "");
        }
    }

    public Respuesta enviarCambioClave(String correo, String clave) {
        String dirActivacion = ("Su clave temporar es:\n\n"
                + clave);

        try {
            // Propiedades de la conexión
            Properties props = new Properties();
            props.setProperty("mail.smtp.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.user", "spaceview2018@gmail.com");
            props.setProperty("mail.smtp.auth", "true");

            // Preparamos la sesion
            Session session = Session.getDefaultInstance(props);

            // Construimos el mensaje
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress("spaceview2018@gmail.com"));
            message.addRecipient(
                    Message.RecipientType.TO,
                    new InternetAddress(correo));
            message.setSubject("Recuperar Clave");
            message.setText(dirActivacion);

            // Lo enviamos.
            Transport t = session.getTransport("smtp");
            t.connect("spaceview2018@gmail.com", "80MK002CUS");
            t.sendMessage(message, message.getAllRecipients());
            // Cierre.
            t.close();
            return new Respuesta(true, "", "");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new Respuesta(false, e.getMessage(), "");
        }
    }

    /**
     *
     * @param correo al que se va a mandar
     * @param nArchvo nombre del archivo adjunto
     * @param mensaje mensaje del correo
     * @param urArchivo direccion donde esta el archivo
     */
    public void enviarAdjunto(String correo, String nArchvo, String mensaje, String urArchivo) {
        try {
            // se obtiene el objeto Session. La configuración es para
            // una cuenta de gmail.
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.user", "spaceview2018@gmail.com");
            props.setProperty("mail.smtp.auth", "true");

            Session session = Session.getDefaultInstance(props, null);
            // session.setDebug(true);

            // Se compone la parte del texto
            BodyPart texto = new MimeBodyPart();
            texto.setText(mensaje);

            // Se compone el adjunto con la imagen
            BodyPart adjunto = new MimeBodyPart();
            adjunto.setDataHandler(
                    new DataHandler(new FileDataSource(urArchivo)));
            adjunto.setFileName(nArchvo);

            // Una MultiParte para agrupar texto e imagen.
            MimeMultipart multiParte = new MimeMultipart();
            multiParte.addBodyPart(texto);
            multiParte.addBodyPart(adjunto);

            // Se compone el correo, dando to, from, subject y el
            // contenido.
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress("spaceview2018@gmail.com"));
            message.addRecipient(
                    Message.RecipientType.TO,
                    new InternetAddress(correo));
            message.setSubject("Documentos");
            message.setContent(multiParte);

            // Se envia el correo.
            Transport t = session.getTransport("smtp");
            t.connect("spaceview2018@gmail.com", "80MK002CUS");
            t.sendMessage(message, message.getAllRecipients());
            t.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String muestraContenido() throws FileNotFoundException, IOException {
        String cadena;
        String dir = "";
        FileReader f = new FileReader("config/properties.ini");
        BufferedReader b = new BufferedReader(f);
        while ((cadena = b.readLine()) != null) {
            dir += cadena;
        }
        b.close();
        dir = dir.replace("resturl=", "");
         dir = dir.replace("propiedades.", "");
        return dir;
    }
}
