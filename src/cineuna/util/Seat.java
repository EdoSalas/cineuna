/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.util;

import cineuna.model.AsientoDto;
import javafx.scene.control.Label;

/**
 *
 * @author liedu
 */
public class Seat extends Label {

    private String fila;
    private Integer numero;
    AsientoDto asiento;

    public Seat() {
    }
    
    public Seat(String fila, Integer numero) {
        this.fila = fila;
        this.numero = numero;
        this.asiento = new AsientoDto();
    }

    public Seat(String fila, Integer numero, String text) {
        super(text);
        this.fila = fila;
        this.numero = numero;
         this.asiento = new AsientoDto();
    }

    

    public String getFila() {
        return fila;
    }

    public void setFila(String fila) {
        this.fila = fila;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public AsientoDto getAsiento() {
        return asiento;
    }

    public void setAsiento(AsientoDto asiento) {
        this.asiento = asiento;
    }

}
