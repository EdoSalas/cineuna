/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.util;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Matami
 */
public class Clave {
     private static Clave INSTANCE = null;
     private ArrayList<String>  abecedario = new ArrayList(Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                                    "K", "L", "M","N","O","P","Q","R","S","T","U","V","W", "X","Y","Z","AD","2D","3D","4D","5D","6D","7D","8D","9D","0D" ,"JD","QD","KD", "AB","2B","3B","4B","5B","6B","7B","8B","9B","0B", "JB","QB","KB" ));
    private static void createInstance() {
        if (INSTANCE == null) {
            synchronized (Clave.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Clave();
                }
            }
        }
    }

    public static Clave getInstance() {
        if (INSTANCE == null) {
            createInstance();
        }
        return INSTANCE;
    }
    public String claveRandom(){
        String nuClave = "";
        for(int i=0; i<6 ;i++){
            int numRandon = (int) Math.round(Math.random() * abecedario.size()-1 );
            nuClave += (abecedario.get(numRandon));
        }
        
        return nuClave;
    }

}
