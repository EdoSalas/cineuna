package cineuna.util;

import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.Window;

/**
 *
 * @author ccarranza
 */
public class Mensaje {

    private Mensaje() {
    }

    public static void show(AlertType tipo, String titulo, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(null);
        alert.setContentText(mensaje);
        alert.show();
    }

    public static void showModal(AlertType tipo, String titulo, Window padre, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(null);
        alert.initOwner(padre);
        alert.setContentText(mensaje);
        alert.showAndWait();

    }

    public static Boolean showConfirmation(String titulo, Window padre, String mensaje) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(titulo);
        alert.setHeaderText(null);
        alert.initOwner(padre);
        alert.setContentText(mensaje);
        Optional<ButtonType> result = alert.showAndWait();

        return result.get() == ButtonType.OK;
    }

    public static String seleccion(String idioma, String usuario) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        ButtonType admin;
        ButtonType cli;
        if (idioma.equalsIgnoreCase("ES")) {
            alert.setTitle("Bienvenido: " + usuario);
            alert.setHeaderText("¿Cómo desea iniciar sesión?");
            alert.setContentText("Seleccione una opción.");

            admin = new ButtonType("Administrador");
            cli = new ButtonType("Cliente");
        } else {
            alert.setTitle("Welcome: " + usuario);
            alert.setHeaderText("How do you want to log in?");
            alert.setContentText("Choose your option.");

            admin = new ButtonType("Administrator");
            cli = new ButtonType("Client");
        }
        
        alert.getButtonTypes().setAll(admin, cli);
        Optional<ButtonType> result = alert.showAndWait();
        String opcion = "Administrador";
        if (result.get() == admin) {
            opcion = "Administrador";
        } else if (result.get() == cli) {
            opcion = "Cliente";
        }
        return opcion;
    }
}
