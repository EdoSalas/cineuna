/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.util;

import com.jfoenix.controls.JFXDrawer;
import java.io.IOException;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import cineuna.main;
import cineuna.controller.Controller;
import javafx.stage.StageStyle;

public class FlowController {

    private static FlowController INSTANCE = null;
    private static Stage mainStage;
    private static ResourceBundle idioma;
    private static HashMap<String, FXMLLoader> loaders = new HashMap<>();

    private FlowController() {
    }

    private static void createInstance() {
        if (INSTANCE == null) {
            // Sólo se accede a la zona sincronizada
            // cuando la instancia no está creada
            synchronized (FlowController.class) {
                // En la zona sincronizada sería necesario volver
                // a comprobar que no se ha creado la instancia
                if (INSTANCE == null) {
                    INSTANCE = new FlowController();
                }
            }
        }
    }

    public static FlowController getInstance() {
        if (INSTANCE == null) {
            createInstance();
        }
        return INSTANCE;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    public void loadLanguage(ResourceBundle lenguaje) {
        FlowController.idioma = lenguaje;
        loaders.clear();
        //goMain();
    }

    public ResourceBundle getLanguage() {
        return FlowController.idioma;
    }

    public void InitializeFlow(Stage stage, ResourceBundle idioma) {
        getInstance();
        this.mainStage = stage;
        this.idioma = idioma;
    }

    private FXMLLoader getLoader(String name) {

        FXMLLoader loader = loaders.get(name);
        if (loader == null) {
            synchronized (FlowController.class) {
                // En la zona sincronizada sería necesario volver
                // a comprobar que no se ha creado la instancia
                if (loader == null) {
                    try {
                        loader = new FXMLLoader(main.class.getResource("view/" + name + ".fxml"), FlowController.idioma);
                        loader.load();
                        loaders.put(name, loader);
                    } catch (Exception ex) {
                        loader = null;
                        java.util.logging.Logger.getLogger(FlowController.class.getName()).log(Level.SEVERE, "Creando loader [" + name + "].", ex);
                    }
                }
            }
        }
        return loader;
    }

    public void goMain() {
        try {
            if (((String) AppContext.getInstance().get("nuevo")) == null) {
                this.mainStage.initStyle(StageStyle.UNDECORATED);
            }
            FlowController.mainStage.resizableProperty().setValue(Boolean.FALSE);
            Scene nueva = new Scene(FXMLLoader.load(main.class.getResource("view/Principal.fxml"), this.idioma));
            FlowController.mainStage.setScene(nueva);
            FlowController.mainStage.show();
            FlowController.mainStage.getScene().setFill(javafx.scene.paint.Color.AQUA);
            mainStage.getIcons().add(new Image("cineuna/resources/favicon.png"));
            if (idioma.equals(ResourceBundle.getBundle("cineuna/resources/i18n/es"))) {
                mainStage.setTitle("Cine UNA");
            } else {
                mainStage.setTitle("Cine UNA");
            }

        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(FlowController.class.getName()).log(Level.SEVERE, "Error inicializando la vista base.", ex);
        }
    }

    public void goLogIng() {
        loaders.clear();
        goViewInWindow("LogIng");
    }

    public void goView(String viewName) {
        goView(viewName, "Center", null);
    }

    public void goView(String viewName, String accion) {
        goView(viewName, "Center", accion);
    }

    public void goView(String viewName, String location, String accion) {
        //crea objeto encargado de animacion 
        Cambio cam = new Cambio();
        //contenedor de la pantalla
        VBox vbPrincipal = (VBox) AppContext.getInstance().get("centro");
        //cambio de pantalla normal
        FXMLLoader loader = getLoader(viewName);
        Controller controller = loader.getController();
        controller.setAccion(accion);
        controller.initialize();
        Stage stage = controller.getStage();
        if (stage == null) {
            stage = this.mainStage;
            controller.setStage(stage);
        }
        switch (location) {
            case "Center":
                ((VBox) ((BorderPane)((StackPane) stage.getScene().getRoot()).getChildren().get(1)).getCenter()).getChildren().clear();
                ((VBox) ((BorderPane)((StackPane) stage.getScene().getRoot()).getChildren().get(1)).getCenter()).getChildren().add(loader.getRoot());
                cam.aniCamPantallaFinal(vbPrincipal);
                break;
            case "Top":
                ((BorderPane) stage.getScene().getRoot()).setTop(loader.getRoot());
                break;
            case "Bottom":
                ((BorderPane) stage.getScene().getRoot()).setBottom(loader.getRoot());
                break;
            case "Right":
                ((BorderPane) stage.getScene().getRoot()).setRight(loader.getRoot());
                break;
            case "Left":
                ((JFXDrawer) ((AnchorPane) ((StackPane) stage.getScene().getRoot()).getChildren().get(1)).getChildren().get(1)).getSidePane().clear();
                ((JFXDrawer) ((AnchorPane) ((StackPane) stage.getScene().getRoot()).getChildren().get(1)).getChildren().get(1)).setSidePane((VBox) loader.getRoot());
                //((BorderPane) stage.getScene().getRoot()).setLeft(loader.getRoot());
                break;
            default:
                ((BorderPane) stage.getScene().getRoot()).setCenter(loader.getRoot());
        }
    }

    public void goViewInStage(String viewName, Stage stage) {
        FXMLLoader loader = getLoader(viewName);
        Controller controller = loader.getController();
        controller.setStage(stage);
        stage.getScene().setRoot(loader.getRoot());
    }

    public void goViewInWindow(String viewName) {
        if (((String) AppContext.getInstance().get("nuevo")) == null) {
            this.mainStage.initStyle(StageStyle.UNDECORATED);
        }
        FXMLLoader loader = getLoader(viewName);
        Controller controller = loader.getController();
        Stage stage = new Stage();
        stage.getIcons().add(new Image("cineuna/resources/favicon.png"));
        if (idioma.equals(ResourceBundle.getBundle("cineuna/resources/i18n/es"))) {
            stage.setTitle("Bienvenido");
        } else {
            stage.setTitle("Welcome");
        }
        stage.setOnHidden((WindowEvent event) -> {
            controller.setStage(null);
        });
        controller.setStage(stage);
        //panel.getChildren().add(loader.getRoot());
        Parent root = loader.getRoot();
        //stage.initStyle(StageStyle.UTILITY);
        stage.resizableProperty().setValue(Boolean.FALSE);
        Scene scene = new Scene(root);
        scene.setFill(javafx.scene.paint.Color.AQUA);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.show();
    }

    public void goViewInWindowModal(String viewName, Stage parentStage, Boolean resizable) {
        //VBox panel = new VBox();
        FXMLLoader loader = getLoader(viewName);
        Controller controller = loader.getController();
        controller.initialize();
        Stage stage = new Stage();
        stage.getIcons().add(new Image("alquilercanchas/resources/favicon.png"));
        stage.setTitle("Sistema de Alquiler Canchas");
        stage.setResizable(resizable);
        stage.setOnHidden((WindowEvent event) -> {
            controller.setStage(null);
        });
        controller.setStage(stage);
        //panel.getChildren().add(loader.getRoot());
        stage.resizableProperty().setValue(Boolean.FALSE);
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(parentStage);
        stage.centerOnScreen();
        stage.showAndWait();

    }

    public Controller getController(String viewName) {
        return getLoader(viewName).getController();
    }

    public void salir() {
        this.mainStage.close();
    }

    /**
     * Elimina un objeto del contexto de la aplicación
     *
     * @param parameter Nombre del objeto
     */
    public void delete(String parameter) {
        loaders.put(parameter, null);
    }

    public static Stage getStageFlow() {
        return FlowController.mainStage;
    }
}
