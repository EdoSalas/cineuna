/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.util;

import cineuna.controller.LogIngController;
import cineuna.model.PeliculaDto;
import cineuna.service.PeliculaService;
import com.jfoenix.controls.JFXButton;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Alert;
import javafx.scene.image.ImageView;

/**
 *
 * @author Eduardo Salas Cerdas
 */
public class TablaPeliculas {

    //Atributos
    private ImageView iv;
    private SimpleStringProperty nombre;
    private SimpleStringProperty genero;
    private SimpleStringProperty activo;
    private SimpleStringProperty descripcion;
    private JFXButton buton;
    private Long id;

    //Constructores
    public TablaPeliculas() {
        this.iv = new ImageView();
        this.nombre = new SimpleStringProperty();
        this.genero = new SimpleStringProperty();
        this.activo = new SimpleStringProperty();
        this.descripcion = new SimpleStringProperty();
        this.buton = new JFXButton();
        this.id = new Long("0");
    }

    public TablaPeliculas(Long id, ImageView img, String nombre, String genero, String activo) {
        this();
        this.id = id;
        this.iv.setImage(img.getImage());
        this.iv.setFitHeight(60);
        this.iv.setFitWidth(60);
        this.nombre.set(nombre);
        this.genero.set(genero);
        this.activo.set(activo);
        if (((String) AppContext.getInstance().get("tipoUsuario")).equals("Administrador")) {
            if (((String) AppContext.getInstance().get("idioma")).equalsIgnoreCase("ES")) {
                this.buton.setText("Editar");
            } else {
                this.buton.setText("Edit");
            }
        } else {
            if (((String) AppContext.getInstance().get("idioma")).equalsIgnoreCase("ES")) {
                this.buton.setText("Ver");
            } else {
                this.buton.setText("View");
            }
        }
        if (((String) AppContext.getInstance().get("tipoUsuario")).equalsIgnoreCase("Cliente")) {
            clickCliente();
        } else {
            clickAdmin();
        }
    }
    
    public TablaPeliculas(Long id, ImageView img, String nombre, String genero, String activo, String descripcion){
        this(id, img, nombre, genero, activo);
        this.descripcion.set(descripcion);
    }
    
    public void clickAdmin() {
        try {
            this.buton.setOnAction((event) -> {
                String idioma = (String) AppContext.getInstance().get("idioma");
                PeliculaService ps = new PeliculaService();
                Respuesta res = ps.getPeliculasList();
                if (res.getEstado()) {
                    List<PeliculaDto> lis = (List<PeliculaDto>) res.getResultado("Peliculas");
                    if (lis != null) {
                        for (int i = 0; i < lis.size(); i++) {
                            if (lis.get(i).getPeId().equals(this.id)) {
                                AppContext.getInstance().set("PeliculaCargada", lis.get(i));
                                i = lis.size();
                                FlowController.getInstance().goView("NuevaPelicula");
                            }
                        }
                    }
                } else {
                    if (idioma.equalsIgnoreCase("ES")) {
                        Mensaje.showModal(Alert.AlertType.ERROR, "Error al obtener la lista de peliculas.", FlowController.getStageFlow(), res.getMensaje());
                    } else {
                        Mensaje.showModal(Alert.AlertType.ERROR, "Failed to get the list of movies.", FlowController.getStageFlow(), res.getMensaje());
                    }
                }
            });
        } catch (Exception ex) {
            Logger.getLogger(LogIngController.class.getName()).log(Level.SEVERE, "Error obteniendo la lista de peliculas.", ex);
        }
    }

    public void clickCliente() {
        try {
            this.buton.setOnAction((event) -> {
                String idioma = (String) AppContext.getInstance().get("idioma");
                PeliculaService ps = new PeliculaService();
                Respuesta res = ps.getPeliculasList();
                if (res.getEstado()) {
                    List<PeliculaDto> lis = (List<PeliculaDto>) res.getResultado("Peliculas");
                    if (lis != null) {
                        for (int i = 0; i < lis.size(); i++) {
                            if (lis.get(i).getPeId().equals(this.id)) {
                                AppContext.getInstance().set("PeliculaCargada", lis.get(i));
                                i = lis.size();
                                FlowController.getInstance().goView("Pelicula");
                            }
                        }
                    }
                } else {
                    if (idioma.equalsIgnoreCase("ES")) {
                        Mensaje.showModal(Alert.AlertType.ERROR, "Error al obtener la lista de peliculas.", FlowController.getStageFlow(), res.getMensaje());
                    } else {
                        Mensaje.showModal(Alert.AlertType.ERROR, "Failed to get the list of movies.", FlowController.getStageFlow(), res.getMensaje());
                    }
                }
            });
        } catch (Exception ex) {
            Logger.getLogger(LogIngController.class.getName()).log(Level.SEVERE, "Error obteniendo la lista de peliculas.", ex);
        }
    }

    public ImageView getImagen() {
        return this.iv;
    }

    public void setIv(ImageView img) {
        this.iv = img;
    }

    public String getNombre() {
        return this.nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public String getGenero() {
        return this.genero.get();
    }

    public void setGenero(String genero) {
        this.genero.set(genero);
    }

    public String getActivo() {
        return this.activo.get();
    }

    public void setActivo(String activo) {
        this.activo.set(activo);
    }

    public String getDescripcion() {
        return this.descripcion.get();
    }

    public void setDescripcion(String descripcion) {
        this.descripcion.set(descripcion);
    }
    
    public JFXButton getButon() {
        return buton;
    }

    public void setButon(JFXButton buton) {
        this.buton = buton;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
