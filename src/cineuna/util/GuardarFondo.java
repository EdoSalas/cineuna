/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;

/**
 *
 * @author Matami
 */
public class GuardarFondo {

    public Boolean guardarImagen(Image im1) {
        Boolean funciono = false;
        try {
            BufferedImage im = SwingFXUtils.fromFXImage(im1, null);
            File archivImagen = new File(direccionG()+"Fondo-mar-reluciente.jpg");
            ImageIO.write(im, "png", archivImagen);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public Image getImagenFondo() {
        Image im = null;
        im = new Image(direccionR() + "Fondo-mar-reluciente.jpg");
        if (im != null) {
            return im;
        } else {
            return null;
        }
    }

    public static String direccionR() {
        File f = new File("prue");
        String rutaca = f.getAbsoluteFile().toString();
        rutaca = rutaca.replace("C", "file:///C");
        rutaca = rutaca.replace("\\", "/");
        rutaca = rutaca.replace("prue", "Imagenes/");
        return rutaca;
    }

    public static String direccionG() {
        File f = new File("prue");
        String rutaca = f.getAbsoluteFile().toString();
        rutaca = rutaca.replace("\\\\C", "C");
        rutaca = rutaca.replace("/", "\\\\");
        rutaca = rutaca.replace("prue", "Imagenes\\");
         rutaca = rutaca.replace("%20", " ");
        return rutaca;
    }
}
