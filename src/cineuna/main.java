/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cineuna;

import cineuna.util.FlowController;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 *
 * @author Eduardo Salas Cerdas
 */
public class main extends Application {
    
    @Override
    public void start(Stage stage) {
        FlowController.getInstance().InitializeFlow(stage, ResourceBundle.getBundle("cineuna/resources/i18n/es"));
        FlowController.getInstance().goViewInWindow("LogIng");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
